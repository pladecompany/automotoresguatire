-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 03-11-2020 a las 18:54:59
-- Versión del servidor: 5.7.31-cll-lve
-- Versión de PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `automot4_guatire`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `usuario` varchar(50) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `correo` varchar(100) DEFAULT NULL,
  `acceso` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `admins`
--

INSERT INTO `admins` (`id`, `usuario`, `password`, `correo`, `acceso`) VALUES
(1, 'Administrador', '5c56c644a006be454a18e658b4e785f4', 'admin@automotoresguatire.com.ve', 0),
(2, 'Citas', '827ccb0eea8a706c4c34a16891f84e7b', 'citas@automotoresguatire.com.ve', 1),
(3, 'Vehículos', '827ccb0eea8a706c4c34a16891f84e7b', 'vehiculos@automotoresguatire.com.ve', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agentes`
--

CREATE TABLE `agentes` (
  `id` int(11) NOT NULL,
  `cod_age` varchar(20) DEFAULT NULL,
  `nom_age` varchar(20) DEFAULT NULL,
  `ape_age` varchar(25) DEFAULT NULL,
  `tlf_age` varchar(15) DEFAULT NULL,
  `img_age` text,
  `est_age` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `agentes`
--

INSERT INTO `agentes` (`id`, `cod_age`, `nom_age`, `ape_age`, `tlf_age`, `img_age`, `est_age`) VALUES
(1, '00001', 'Leonardo', 'Diaz', '04120545738', 'https://exiauto.com/static/img/user.png', 1),
(2, '00002', 'Ivan', 'Toro', '04120545738', 'https://exiauto.com/static/img/user.png', 1),
(3, '00003', 'Danny', 'Ascanio', '04120545738', 'https://exiauto.com/static/img/user.png', 1),
(4, '00004', 'Marlon', 'Ramirez', '', 'https://exiauto.com/static/img/user.png', 1),
(5, '00005', 'Alex', 'Garcia', '', 'https://exiauto.com/static/img/user.png', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias_repuestos`
--

CREATE TABLE `categorias_repuestos` (
  `id` int(11) NOT NULL,
  `nom_cat` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `citas`
--

CREATE TABLE `citas` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `id_vehiculo` int(11) DEFAULT NULL,
  `id_agente` int(11) DEFAULT NULL,
  `motivo` text,
  `fecha` datetime DEFAULT NULL,
  `estatus` int(11) DEFAULT NULL,
  `fec_env` datetime DEFAULT NULL,
  `fac_res` datetime DEFAULT NULL,
  `tipo_cita` varchar(25) DEFAULT NULL,
  `id_kilometros` int(11) DEFAULT NULL,
  `id_falla` int(11) DEFAULT NULL,
  `observacion` text,
  `reprogramado` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentarios_reclamos`
--

CREATE TABLE `comentarios_reclamos` (
  `id` int(11) NOT NULL,
  `id_reclamo` int(11) DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `id_admin` int(11) DEFAULT NULL,
  `comentario` text,
  `fec_com` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamentos`
--

CREATE TABLE `departamentos` (
  `id` int(11) NOT NULL,
  `nom_dep` varchar(50) DEFAULT NULL,
  `des_dep` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `departamentos`
--

INSERT INTO `departamentos` (`id`, `nom_dep`, `des_dep`) VALUES
(1, 'Vehículos', ''),
(2, 'Repuestos', ''),
(3, 'Servicios', ''),
(4, 'ELP', ''),
(5, 'Otros', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fallas`
--

CREATE TABLE `fallas` (
  `id` int(11) NOT NULL,
  `falla` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `fallas`
--

INSERT INTO `fallas` (`id`, `falla`) VALUES
(1, 'Revisión según Kilometraje (Diagnóstico)'),
(20, 'Alineación'),
(30, 'Balanceo'),
(40, 'Solo Cambio de Aceite y Filtro'),
(50, 'Revisión de Frenos'),
(60, 'Diagnóstico Tren Delantero'),
(70, 'Diagnóstico A/Acondicionado'),
(80, 'Diagnóstico Recalentamiento de Motor'),
(90, 'Revisión Falla de Encendido'),
(100, 'Limpieza de Inyectores'),
(110, 'Diagnóstico de Tripoides'),
(120, 'Diagnóstico de Amortiguadores'),
(130, 'Revisar Fuga de Aceite de Motor'),
(140, 'Revisar Ruido de Rodamiento'),
(150, 'Revisar Luz Encendida en el Tablero'),
(160, 'Revisión y Diagnóstico de Embrague'),
(170, 'Revisión y Diagnóstico de Refrigerante'),
(180, 'Revisar Alternador'),
(190, 'Revisar Arranque'),
(200, 'Revisión y Diagnóstico Ruido en terreno Terrenal'),
(201, 'No aplica');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `kilometrajes`
--

CREATE TABLE `kilometrajes` (
  `id` int(11) NOT NULL,
  `kilometros` varchar(100) DEFAULT NULL,
  `tipo_kil` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `kilometrajes`
--

INSERT INTO `kilometrajes` (`id`, `kilometros`, `tipo_kil`) VALUES
(1, 'No aplica', ''),
(2, '1.000 con garantía', 'gar'),
(3, '10.000 con garantía', 'gar'),
(4, '1.000 sin garantía', 'per'),
(5, '5.000 Kilometros', 'per'),
(60, '10.000 Kilometros', 'per'),
(65, '15.000 Kilometros', 'per'),
(70, '20.000 Kilometros', 'per'),
(75, '25.000 Kilometros', 'per'),
(80, '30.000 Kilometros', 'per'),
(85, '35.000 Kilometros', 'per'),
(90, '40.000 Kilometros', 'per'),
(95, '45.000 Kilometros', 'per'),
(100, '50.000 Kilometros', 'per'),
(105, '55.000 Kilometros', 'per'),
(110, '60.000 Kilometros', 'per'),
(115, '65.000 Kilómetros', 'per'),
(120, '70.000 Kilómetros', 'per'),
(125, '75.000 Kilómetros', 'per'),
(130, '80.000 Kilómetros', 'per'),
(135, '85.000 Kilómetros', 'per'),
(140, '90.000 Kilómetros', 'per'),
(145, '95.000 Kilómetros', 'per'),
(150, '100.000 Kilómetros', 'per'),
(155, '105.000 Kilómetros', 'per'),
(160, '110.000 Kilómetros', 'per'),
(165, '115.000 Kilómetros', 'per'),
(170, '120.000 Kilómetros', 'per'),
(175, '125.000 Kilómetros', 'per'),
(180, '130.000 Kilómetros', 'per'),
(185, '135.000 Kilómetros', 'per'),
(190, '140.000 Kilómetros', 'per'),
(195, '145.000 Kilómetros', 'per'),
(200, '150.000 Kilómetros', 'per'),
(205, '155.000 Kilómetros', 'per'),
(210, '160.000 Kilómetros', 'per'),
(215, '165.000 Kilómetros', 'per'),
(220, '170.000 Kilómetros', 'per'),
(225, '175.000 Kilómetros', 'per'),
(230, '180.000 Kilómetros o Superior', 'per');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `misvehiculos`
--

CREATE TABLE `misvehiculos` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `id_modelo` int(11) DEFAULT NULL,
  `placa` varchar(20) DEFAULT NULL,
  `serial1` varchar(40) DEFAULT NULL,
  `serial2` varchar(40) DEFAULT NULL,
  `serial3` varchar(40) DEFAULT NULL,
  `serial4` varchar(40) DEFAULT NULL,
  `ano` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modelos`
--

CREATE TABLE `modelos` (
  `id` int(11) NOT NULL,
  `modelo` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `modelos`
--

INSERT INTO `modelos` (`id`, `modelo`) VALUES
(1, 'COROLLA'),
(2, 'CAMRY'),
(5, 'Yaris'),
(6, 'Fortuner'),
(7, 'hiace'),
(8, 'Hilux'),
(10, 'LAND CRUISER');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modelos_citas`
--

CREATE TABLE `modelos_citas` (
  `id` int(11) NOT NULL,
  `modelo` text,
  `transmision` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `modelos_citas`
--

INSERT INTO `modelos_citas` (`id`, `modelo`, `transmision`) VALUES
(1, 'Yaris', 'Automatica'),
(2, 'Camry', 'Manual'),
(3, 'Corolla', 'Automatica'),
(5, '4Runner', 'Automatica'),
(6, 'Fortuner', 'Automática'),
(7, 'Hiace', NULL),
(8, 'Hilux', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

CREATE TABLE `noticias` (
  `id` int(11) NOT NULL,
  `titulo` varchar(100) DEFAULT NULL,
  `descripcion` longtext,
  `img` text,
  `fec_reg_noti` date DEFAULT NULL,
  `est_noti` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `noticias`
--

INSERT INTO `noticias` (`id`, `titulo`, `descripcion`, `img`, `fec_reg_noti`, `est_noti`) VALUES
(4, 'Conoce nuestros servicios', 'Conoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios aConoce nuestros servicios a', 'http://pladecompany.com/demos/exiauto.com.ve/static/img/files/2019_08_27_11_27_41mantenimiento.jpg', '2019-08-27', 0),
(5, 'Latoneria y pintura una de nuestras especialidades', 'Latoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidadesLatoneria y pintura una de nuestras especialidades', 'https://exiauto.com/static/img/files/2019_09_11_12_47_062019_08_27_11_27_22latoneria.jpg', '2019-08-27', 0),
(7, 'La Ruta del Llamado Toyota', 'Exiauto Forma parte de La Ruta del Llamado Toyota.\r\nAlgunas Bolsas de Aire pudiesen presentar un defecto en el inflador, que podr&iacute;a ocasionar graves lesiones. Ven a Exiauto y reemplazaremos el inflador por uno nuevo; es totalmente gratis, r&aacute;pido y f&aacute;cil.\r\n\r\nMientras aplicamos el Llamado, podr&aacute;s disfrutar de la exhibici&oacute;n de veh&iacute;culos, foodtrucks, m&uacute;sica y un &aacute;rea exclusiva para los m&aacute;s peque&ntilde;os y seguridad.\r\n\r\nviernes y s&aacute;bado: 8:00 am - 5:00 pm\r\ndomingo: 9:00 am - 3:00 pm \r\n\r\n&iexcl;No te arriesgues! Ven a La Ruta ????.', 'https://exiauto.com/static/img/files/2019_09_11_12_46_342019_09_10_13_13_21Ruta_LLamado.jpg', '2019-09-10', 1),
(8, 'El Toyota Yaris Hatchback 2020 para Japón obtiene AWD, plataforma completamente nueva', '\r\nEl Toyota Yaris est&aacute; muerto en Estados Unidos, pero vivir&aacute; en todo el resto del mundo. Todav&iacute;a obtenemos la versi&oacute;n angular, basada en Mazda, que est&aacute; identificada como Toyota, pero esta nueva que obtendr&aacute; el resto del mundo es un poco m&aacute;s completa y casi completamente diferente. El nuevo Yaris en el extranjero se basa en la plataforma de Nueva Arquitectura Global (TNGA) de Toyota y cuenta con tres opciones de tren motriz, mientras que la versi&oacute;n que tenemos en Estados Unidos solo tiene una.\r\nLa plataforma TNGA se usa por primera vez en un autom&oacute;vil subcompacto, y Toyota dice que a partir de ahora basar&aacute;n todos sus autom&oacute;viles subcompactos en esta plataforma. El uso de esta plataforma ayuda a darle al Yaris un poco m&aacute;s de espacio en la cabina junto con una suspensi&oacute;n actualizada y un cuerpo que es 100 libras m&aacute;s ligero.\r\n \r\nEl 1.5 litros de tres cilindros est&aacute; disponible con un nuevo sistema h&iacute;brido que proviene de los m&aacute;s grandes que se encuentran en el Corolla, RAV4 y Camry. Se ofrece con el sistema el&eacute;ctrico de tracci&oacute;n total E-Four de Toyota, otra primicia para los autos subcompactos de Toyota. Toyota a&uacute;n no ha reclamado los n&uacute;meros de potencia, pero s&iacute; dice que el h&iacute;brido tiene un 15 por ciento m&aacute;s de potencia combinada y un aumento de m&aacute;s del 20 por ciento en la eficiencia del combustible. Tambi&eacute;n hay disponible una versi&oacute;n a gasolina del motor de 1.5 litros y uno de tres cilindros de 1.0 litro. En los Estados Unidos, el sed&aacute;n Yaris con sede en Mazda viene de serie con una transmisi&oacute;n manual, aunque el hatchback solo est&aacute; disponible con una transmisi&oacute;n autom&aacute;tica de seis velocidades. En la nueva escotilla Yaris que no obtendremos, se ofrece un manual de seis velocidades con el motor de gasolina de 1.5 litros. Una transmisi&oacute;n continua de cambio continuo con tracci&oacute;n delantera o tracci&oacute;n total est&aacute; disponible para el 1.5 litros, y un CVT con tracci&oacute;n delantera est&aacute; disponible para el 1.0 litro.\r\nLa bater&iacute;a de hidruro de n&iacute;quel-metal que se encontraba en el Yaris de anta&ntilde;o ha sido reemplazada por una bater&iacute;a h&iacute;brida de iones de litio, que seg&uacute;n Toyota puede ayudar a aumentar la aceleraci&oacute;n. La nueva bater&iacute;a tambi&eacute;n es un 27 por ciento m&aacute;s ligera que la que reemplaza.\r\nToyota Safety Sense es est&aacute;ndar en el nuevo Yaris, presentando el debut de un sistema de asistencia de estacionamiento, Advanced Park, que es nuevo para Toyota, y un sistema de detecci&oacute;n de autos que se aproxima, otro primero; Tambi&eacute;n incluye las caracter&iacute;sticas de seguridad activa esperadas, como control de crucero adaptativo y asistencia de mantenimiento de carril. Una pantalla frontal de color de 10 pulgadas tambi&eacute;n es est&aacute;ndar.\r\nEl Toyota Yaris 2020 estar&aacute; en exhibici&oacute;n en el Auto Show de Tokio esta semana y a la venta en Jap&oacute;n a partir de mediados de febrero de 2020.\r\n\r\n', 'https://exiauto.com/static/img/files/2019_10_24_11_28_14toyota2.jpg', '2019-10-24', 1),
(9, 'Procedimientos que debes realizar con profesionales como Exiauto, y no por tu cuenta. ', 'Hay procedimientos que pueden observarse como sencillos para el usuario, pero por su real dificultad o consecuencias que puede desencadenar en tu veh&iacute;culo, te recomendamos hacerlas con un profesional:\r\n\r\n1. Cambiar el aceite y su filtro: En esta operaci&oacute;n se observa como muy simple para los usuarios, sin embargo hay que tener en cuenta el aceite adecuado para tu veh&iacute;culo, el estatus, y la gesti&oacute;n de los residuos como marca la ley.\r\n\r\n2. Cambiar las pastillas y discos de freno: Hay que tener en cuenta que los frenos son unos de los elementos de seguridad m&aacute;s importantes del vehiculo, por eso este cambio debe realizarlo un profesional. Adem&aacute;s de los conocimientos, hay que tener todos las herramientas necesarias.\r\n\r\n3. Cambiar la distribuci&oacute;n (correa o kit) y la bomba de agua: Se trata de una operaci&oacute;n m&aacute;s compleja, que de no realizarse adecuadamente, puede da&ntilde;ar el motor de tu veh&iacute;culo.\r\n\r\n4. Recargar el aire acondicionado:  Existen varios tipos de gas y utilizar uno que no sea el adecuado puede resultar peligroso. Hay que tener en cuenta que el gas es inflamable y su manipulaci&oacute;n es muy peligrosa.\r\n\r\n5. Cambiar l&iacute;quido de frenos: Es una tarea que puede resultar complicada y que un profesional puede realizar sin problemas.', 'https://exiauto.com/static/img/files/2019_11_26_09_56_07toyota_VOLANTE.jpg', '2019-11-26', 1),
(10, '¿Conoces la nueva Toyota Hilux GR Sport?', 'Toyota presenta la Hilux GR Sport, una nueva versi&oacute;n de Toyota GAZOO Racing.\r\nLa nueva Hilux GR Sport suma tambi&eacute;n equipamiento funcional pensado para brindarle una experiencia superadora al conductor off-road.\r\nEquipada con un motor V6 naftero de 238 CV de potencia m&aacute;xima, una caja autom&aacute;tica de 6 velocidades.\r\nEste conjunto entrega una aceleraci&oacute;n m&aacute;s en&eacute;rgica, con reacciones m&aacute;s r&aacute;pidas, brind&aacute;ndole al veh&iacute;culo un acentuado car&aacute;cter deportivo que resulta en una conducci&oacute;n m&aacute;s emocionante.\r\n', 'https://exiauto.com/static/img/files/2020_02_04_13_55_34gr_sport-3.jpg', '2020-02-04', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reclamos`
--

CREATE TABLE `reclamos` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `fec_reg_rec` datetime DEFAULT NULL,
  `mensaje` text,
  `est_rec` int(1) DEFAULT NULL,
  `tipo` varchar(1) DEFAULT NULL,
  `id_departamento` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `repuestos`
--

CREATE TABLE `repuestos` (
  `id` int(11) NOT NULL,
  `id_categoria` int(11) DEFAULT NULL,
  `nom_rep` varchar(100) DEFAULT NULL,
  `des_rep` text,
  `ano_rep` int(11) DEFAULT NULL,
  `pre_rep` float DEFAULT NULL,
  `cam_1` varchar(100) DEFAULT NULL,
  `cam_2` varchar(100) DEFAULT NULL,
  `cam_3` varchar(100) DEFAULT NULL,
  `img1` text,
  `img2` text,
  `img3` text,
  `img4` text,
  `img5` text,
  `est_rep` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitud_repuestos`
--

CREATE TABLE `solicitud_repuestos` (
  `id` int(11) NOT NULL,
  `nom` varchar(30) DEFAULT NULL,
  `ape` varchar(30) DEFAULT NULL,
  `tel` varchar(15) DEFAULT NULL,
  `cor` varchar(100) DEFAULT NULL,
  `repuesto` text,
  `fec_reg_sol` datetime DEFAULT NULL,
  `estatus` int(1) DEFAULT NULL,
  `fec_pro_sol` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `solicitud_repuestos`
--

INSERT INTO `solicitud_repuestos` (`id`, `nom`, `ape`, `tel`, `cor`, `repuesto`, `fec_reg_sol`, `estatus`, `fec_pro_sol`) VALUES
(1, 'carlos', 'castellanos', '04120545738', 'castellanos@pladecompany.com', 'Esta es una prueba de las solicitudes de repuestos desde la pagina web.', '2020-02-18 16:32:08', 1, NULL),
(2, 'Andrey ', 'Colmenares ', '04125747451 ', 'andrycolmenares24@hotmail.com ', 'Espirales delantero y trasero de una hilux doble cabina 2.7 año 2007 ', '2020-02-19 11:45:24', 1, NULL),
(3, 'Alys', 'Alfonzo ', '04148080344', 'alfonzoalys@gmail.com ', 'Control de mando eleva vidrios electrónico de puerta de piloto Yaris Belta año 2007 ', '2020-02-19 14:37:56', 1, NULL),
(4, 'Alys', 'Alfonzo ', '04148080344', 'alfonzoalys@gmail.com ', 'Control de mando eleva vidrios electrónico de puerta de piloto Yaris Belta año 2007 ', '2020-02-19 14:38:04', 1, NULL),
(5, 'RAFAEL', 'ALVAREZ', '04122862541', 'info@espacioconvalor.com', 'REGULADOR DE ALTERNADOR 4 PINES PARA YARIS 3 PUERTAS 2007 ', '2020-02-20 14:08:27', 1, NULL),
(6, 'JOSEFINA ', 'SANCHEZ ', '04142209965', 'josefinasanchezoyuela@gmail.com', 'El flotante de la Bomba de Gasolina , Camry 1998', '2020-02-20 15:30:35', 1, NULL),
(7, 'Daniela', 'Egui', '04241760238', 'eguirubiodaniela@yahoo.com.ve', 'Filtros de aceite, gasolina y aire para Toyota Corolla 2010', '2020-02-26 09:00:38', 1, NULL),
(8, 'celimar', 'moreno', '04141357245', 'celimarmccs@gmail.com', 'buenas tards por favor deseo saber si tiene anillos de baiby canry carburado motor 1.6 a 0.40\n', '2020-02-26 12:24:38', 1, NULL),
(9, 'celimar', 'moreno', '04141357245', 'celimarmccs@gmail.com', 'buenas tards por favor deseo saber si tiene anillos de baiby canry carburado motor 1.6 a 0.40\n', '2020-02-26 12:24:38', 1, NULL),
(10, 'celimar', 'moreno', '04141357245', 'celimarmccs@gmail.com', 'buenas tards por favor deseo saber si tiene anillos de baiby canry carburado motor 1.6 a 0.40\n', '2020-02-26 12:24:38', 1, NULL),
(11, 'celimar', 'moreno', '04141357245', 'celimarmccs@gmail.com', 'buenas tards por favor deseo saber si tiene anillos de baiby canry carburado motor 1.6 a 0.40\n', '2020-02-26 12:24:38', 1, NULL),
(12, 'celimar', 'moreno', '04141357245', 'celimarmccs@gmail.com', 'buenas tards por favor deseo saber si tiene anillos de baiby canry carburado motor 1.6 a 0.40\n', '2020-02-26 12:24:38', 1, NULL),
(13, 'Rafael', 'Toro', '04149213982', 'tororafael@gmail.com', 'Para previa 2009. NO Smart\nBases de motor y caja. Todas.\nCorrea Unica y tensor.\nEstopera trasera de sigueñal.\nFiltro Aceite.', '2020-02-27 08:54:04', 1, NULL),
(14, 'Fabian', 'Alvarez', '04143226012', 'fabian.alvarez@aig-ca.com.ve', 'Solicitó la rolinera de aguja del transfer para hilux 2.7 año 2008, 2.7 4x4', '2020-02-27 09:59:46', 1, NULL),
(15, 'franklin ', 'rivero', '04141876276', 'frgofc@gmail.com', 'buenos días \nando buscando las 4 gomas de los tripoides para un toyota corolla 2009 GLI si me podrían ayudar sería buenísimo y muchas gracias ', '2020-02-28 09:23:42', 1, NULL),
(16, 'José  Alejan', 'Pinto Arno', '04242281302', 'Josepintoa@gmail.com', 'Guía trasera izquierda, del parachoque de una 4 Runner, 2008', '2020-02-28 11:07:50', 1, NULL),
(17, 'Omar ', 'rios ', '0412 8779734', 'Omarsk_8@hotmail.com', 'kit cadena de tiempo yaris belta motor 1.5', '2020-02-28 14:22:42', 1, NULL),
(18, 'Ramiro', 'Aliaga', '04143101220', 'ramiroaliaga1973@hotmail.com', 'Necesito para una Merú 2008 las barras tensoras superiores de la transmision trasera, son las barras cortas(las superiores) creo que su nro de pieza es 48710 ', '2020-03-02 14:36:38', 1, NULL),
(19, 'DOMINGO', 'MIJARES', '040142319078', 'domingomijares27@gmail.com', 'AMORTIGUADORES DELANTEROS COROLLA 2010.\nGRACIAS ', '2020-03-03 16:00:08', 1, NULL),
(20, 'Maikel ', 'Tarezian ', '04241877396', 'maikelter13@hotmail.com', 'Buenas noches disponen \nDe los siguientes repuestos originales para Yaris 2007\n\n*Termostato\n*Rodamiento delantero imantados con ABS \n\nGracias ', '2020-03-09 22:41:26', 1, NULL),
(21, 'Cesar', 'Barrios', '04123079234', 'cbarrios58@gmail.com', 'Correa de alternador para Yaris 2002 1.3 sincrónico ', '2020-03-11 15:49:39', 1, NULL),
(22, 'Tomás ', 'Blohm', '0414 3174818', 'tfblohm61@yahoo.es', 'Busco motor de arranque para FJ40 1984', '2020-04-07 13:34:43', 1, NULL),
(23, 'Dario', 'Brillembourg', '04141321885', 'fb.dario@gmail.com', 'Fusible Alternador 100amp TOYOTA PRADO 2004', '2020-04-18 06:30:57', 1, NULL),
(24, 'Jose', 'MORA', '04149022565', 'jamv2507@gmail.com', 'Necesito cotizar un repuesto. Modalidades de pago. Repuesto \nAlternador Toyota Corolla 2001. Motor 1.6', '2020-04-28 18:06:48', 1, NULL),
(25, 'Ma Aurora', 'PANDO', '04142259209', 'yoyazulita@hotmail.com', 'Buen día,\nTienen las pastillas de frenos para Corolla 2005 y precio? Gracias', '2020-05-26 18:43:15', 1, NULL),
(26, 'Gilcar', 'Revolledo', '04242752247', 'gilcar.revolledo@gmail.com', 'Necesito alternador original para un Fj40 ano 73-78. o alguna referencia donde conseguirlo. Gracias', '2020-06-01 11:00:39', 1, NULL),
(27, 'irvin', 'gudiño', '04265198512', 'irvin.gudino3@gmail.com', 'Buenos dias para cotizar las cuatro bases del motor para el Corolla 2010.\nGracias', '2020-06-03 11:31:39', 1, NULL),
(28, 'irvin', 'gudiño', '04265198512', 'irvin.gudino3@gmail.com', 'Buenos dias, para cotizar las cuatro bases del motor para corolla automatico 2010.\ngracias.', '2020-06-03 11:37:04', 1, NULL),
(29, 'Raul', 'Martinez', '0412 260 22 72', 'ramartinez005@gmail.com', 'Bomba de freno de la camioneta Van  Hiace ', '2020-06-07 21:25:28', 1, NULL),
(30, 'Ma Aurora ', 'Pando', '04142259209', 'yoyazulita@hotmail.com', 'Buen día, tienen pastillas de frenos para Corolla 2005? ', '2020-06-08 18:28:40', 1, NULL),
(31, 'Ricardo', 'Quilen', '0424-1469539', 'rquilen@gmail.com', 'Buenas noches.\nSon dos repuestos:\nYARIS AÑO 2000: Sensor de temperatura que activa electro ventilador.\nCOROLLA AÑO 2001: Flotante tanque de gasolina ', '2020-06-09 19:55:12', 1, NULL),
(32, 'ANTONIO', 'MORALES', '04143311021', 'antoniomoraless@hotmail.com', 'MOTOR DEL ELECTROVENTILADOR DEL RADIADOR\nTOYOTA COROLLA 18 AUTOMATICO AÑO 1997\n', '2020-06-16 12:09:22', 1, NULL),
(33, 'Carlis ', 'Rivas', '04242093227', 'crivas.mbglegal@gmail.com', 'Conjunto extremo cremallera (rotula)\nBarra acoplamiento derecha\nBarra acoplamiento izquierda\nAmortiguadores delanteros\nZapata derecha #1\nZapata izquierda #1\nBuje brazo contro inferior tras. der/izq\nZapata derecha #2\nZapata izquierda #2\nBuje barra estabilizadora \nCrucetas\nAmortiguadores traseros der/iz', '2020-06-17 10:43:31', 1, NULL),
(34, 'ramon', 'low', '5648292', 'ramonlow13@gmail.com', 'manguera bomba dirección hidráulica Autana 2002 la pequeña de abajo ', '2020-06-17 14:48:02', 1, NULL),
(35, 'Jose ', 'Martinez', '04166871137', 'jose.martinez3777@gmail.com', '1 Inyector y 2 conectores para Toyota Yaris año 2000', '2020-06-18 18:24:14', 1, NULL),
(36, 'hector ', 'tortolero', '0414-3390040', 'hector.tor.52@gmail.com', 'moldura antigoteo de techo yaris belta 2007 nr. de parte 75552-52140 75551-52160 o 75552-130 mide entre 1.49 y 1.50 mts largo ', '2020-06-30 10:13:03', 1, NULL),
(37, 'María A', 'Castillo', '04243230886', 'mariasancastillo@hotmail.com', 'Distribución Toyota Starlet Año 1998 motor 2E', '2020-07-07 14:28:04', 1, NULL),
(38, 'María a', 'Castillo', 'O4243230886', 'mariasancastillo@hotmail.com', 'Distribución Toyota Starlet Año 1997 motor 2E', '2020-07-07 14:32:07', 1, NULL),
(39, 'Luis ', 'Pulido ', '0416.3119839', 'Luis.pulidop@hotmail.com ', 'Regulador de alternador Toyota fortuner año 2008. Código pieza 126000-3720.', '2020-07-24 22:12:57', 1, NULL),
(40, 'Perla ', 'Martinez', '0414-236.67.30', 'pmartinez@unirent.com.ve', 'Manguera inferior (Nro. 2) de radiador para Hilux 4.0 año 2017 ensamblada en Venezuela, tengo 3 número de parte: 16572-0C070, 16572-0P030 y 16572-0C090\n\nGracias de antemano,', '2020-07-30 10:08:22', 1, NULL),
(41, 'gabriel', 'gonzalez', '04241644265', 'gagb248@gmail.com', 'radio original toyota para un corolla 2012\nel modelo que viene con Bluetooth \npor favor contactar via email', '2020-08-03 01:10:04', 1, NULL),
(42, 'Nila', 'Yáñez ', '04123180391', 'Nyanezg03@gmail.com ', 'Bombillo luz baja corolla 2011', '2020-08-03 14:26:11', 1, NULL),
(43, 'juan', 'marques', '04143204988', 'jmarques@meister.com.ve', 'Buenos días, necesito un gato y los accesorios de una hilux', '2020-08-11 11:02:52', 1, NULL),
(44, 'Ninsoka Caro', 'Velasquez', '04166282325', 'carolavelco@yahoo.com', 'Pila original bomba de gasolina Corolla 2010.\nCodigo 23220-22181. Gracias.', '2020-08-19 10:26:45', 1, NULL),
(45, 'Cesar', 'Rodriguez', '04122004203', 'jarias123@gmail.com', 'Pastillas de freno delanteras y traseras para CAMRY SE 2017? Gracias', '2020-08-20 17:41:24', 1, NULL),
(46, 'Robert ', 'Rodriguez ', '04241260366', 'robertluis2005@gmail.com ', 'Para una Meru 2008\nKit de tiempo\nJuego de empacaduras \nAnillos Std\nCorrea de Alternador\nCables de bujía\nBujias\nFiltro de aceite\nRefrigerante', '2020-08-21 11:55:58', 1, NULL),
(47, 'Yoniray', 'Zambrano', '04125631217', 'yoniray@gmail.com', 'Manillas Internas Corolla 2002 ', '2020-08-25 13:12:56', 1, NULL),
(48, 'Gabriela ', 'Baute', '04141394576', 'gabriela31_6@hotmail.com ', 'Bomba de gasolina para runner 2008 el número de pieza es 77020 - 35071\n       Gracias!!', '2020-08-26 14:40:51', 1, NULL),
(49, 'jose luis', 'Perisse', '0414 1800198', 'jose.luis.perisse@gmail.com', 'Requiero\nMesetas con sus bujes para Toyota Previa\nnecesito disponibilidad y costo', '2020-09-04 12:00:34', 1, NULL),
(50, 'Antonio', 'Ziegler', '04123268056', 'antonioziegler@hotmail.com', 'cajetín de dirección Meru y croche completo Meru.', '2020-09-07 17:54:24', 1, NULL),
(51, 'frrr', 'fffff', '12424523658', 'dfgdxrgx@glrdjg.com', '<sukfhsiudfh', '2020-09-08 12:07:07', 1, NULL),
(52, 'María ', 'MONTILLA GON', '04143116390', 'marelimon@gmail.com', 'Buenas tardes: \nNecesito los siguientes repuestos para un New sensation 2003\n-lápices (2)\n- amortiguadores delanteros (2) y traseros(2)\n-Bujes grandes (2) para meseta delanteros\n\nSaludos', '2020-09-09 15:58:04', 1, NULL),
(53, 'Jenny', 'Ramírez ', '04242582123', 'terranjenny@hotmail.com', 'Requiero costo de las pastillas delanteras y traseras de una 4runner año 2015.\n\nY costo de instalación de mano de obra por instalación de pastillas.\n\nGracias', '2020-09-13 16:36:57', 1, NULL),
(54, 'Agustín ', 'Ascanio', '0424.2050989', 'agustinascanio@gmail.com', 'Clutch Terios 2005. (Plato. collarín y disco)', '2020-09-21 09:02:23', 1, NULL),
(55, 'Adolfo', 'Salazar', '04149612313', 'aesb2000@gmail.com', 'Stop trasero derecho, Terios Bego 2008\nFiltro de aire\nFiltro de gasolina', '2020-09-23 20:45:55', 1, NULL),
(56, 'Antonio', 'Guercio', '04143203647', 'frasavini@gmail.com', 'Compresor A/A Previa 2007', '2020-09-24 14:48:21', 1, NULL),
(57, 'Rafael ', 'Liscano', '0414 5940534', 'rliscanov@gmail.com', 'Motor de arranque de Corolla automático año modelo 1998 motor 1.8 serial # AE1029509500\nFavor informar si tienen en existencia y precio del mismo. este repuesto lo requiero con urgencia.\nGracias, por la ayuda.\nRafael Liscano', '2020-09-27 19:25:27', 1, NULL),
(58, 'yosselys', 'pinto', '04263106785', 'yosman99@hotmail.com', 'concha de biela con cuñas 0,10\nconcha de bancada 0,1 con cuñas y otras sin cuñas es decir 5 con cuñas y 5 sin cuñas\nbomba de aceite\nkit de cadena de tiempo\nbases de  motor\nterios año 2007 motor 1,3 sincronica 4X4', '2020-09-29 16:32:54', 1, NULL),
(59, 'marco', 'fiorilli', '04241598811', 'mfiorilli@ronava.com', 'flotante gasolina Y BUJIAS DEL  Baby cAMTY 1.6 CARBURADO.', '2020-09-30 10:26:26', 1, NULL),
(60, 'Ingrid', 'Angelastro ', '04123059458', 'Ingridangelastro1@gmail.com', 'Frenos delanteros previa 2007', '2020-10-07 11:45:33', 1, NULL),
(61, 'Jhon', 'Montes de Oc', '0414 1237802', 'montesj_07@hotmail.com', 'Bases yaris sport automático 2008\nMotor trasera\nMotor derecha\nCaja izquierda\n', '2020-10-09 00:03:48', 1, NULL),
(62, 'CARLOS', 'FIGUEROA', '0414 2404806', 'carfi57@gmail.com', 'Buenos días, me interesa conocer los precios de los siguientes repuestos y, al mismo tiempo, saber si ustedes aceptan pagos via Zelle\nGracias por su atención\nToyota Corolla 1.8 año 2000 automático\n-- Kit de empacaduras del motor\n-- Kit correa de tiempos (tensor, patin y correas)\n-- Bomba de aceite marca Aisin original\n', '2020-10-16 11:05:16', 1, NULL),
(63, 'Mauro', 'Farabegoli', '04122396546', 'Farabem@gmail.com', 'Vacuum avance distribuidor\nToyota Corolla 1995 (Baby Camry) carburado. \nGracias ', '2020-10-21 11:15:18', 1, NULL),
(64, 'domingo ', 'utrera', '04125330267', 'angeldomingoutrera@gmail.com', 'árbol de leva (admisión ) para toyota previa 2009, motor cuatro cilindros', '2020-10-22 21:18:28', 1, NULL),
(65, 'Freddy', 'Peña', '04166382909', 'fpena02@gmail.com', 'Kits para el cajetín de de la dirección de la Meru 2008\nEstopera para la rueda trasera Izquierda de la Meru 2008', '2020-10-26 15:06:02', 1, NULL),
(66, 'Brigitte', 'Rodríguez', '04242726848', 'brigitter2809@gmail.com', 'Hola buenos días ! Quisiera saber si tienen disponible aspa de electroventilador de Terios Bego 2009.', '2020-10-29 09:03:57', 1, NULL),
(67, 'Brigitte', 'Rodríguez', '04242726848', 'brigitter2809@gmail.com', 'Hola buenos días ! Quisiera saber si tienen disponible aspa de electroventilador de Terios Bego 2009.', '2020-10-29 09:04:06', 1, NULL),
(68, 'Brigitte', 'Rodríguez', '04242726848', 'brigitter2809@gmail.com', 'Hola buenos días ! Quisiera saber si tienen disponible aspa de electroventilador de Terios Bego 2009.', '2020-10-29 09:04:11', 1, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `ced_usu` varchar(15) DEFAULT NULL,
  `nom_usu` varchar(30) DEFAULT NULL,
  `ape_usu` varchar(35) DEFAULT NULL,
  `cor_usu` varchar(100) DEFAULT NULL,
  `tel_usu` varchar(15) DEFAULT NULL,
  `pas_usu` varchar(500) DEFAULT NULL,
  `fec_nac_usu` date DEFAULT NULL,
  `fec_reg_usu` datetime DEFAULT NULL,
  `emp_usu` varchar(100) DEFAULT NULL,
  `facebook` varchar(40) DEFAULT NULL,
  `instagram` varchar(40) DEFAULT NULL,
  `twitter` varchar(40) DEFAULT NULL,
  `img_usu` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `ced_usu`, `nom_usu`, `ape_usu`, `cor_usu`, `tel_usu`, `pas_usu`, `fec_nac_usu`, `fec_reg_usu`, `emp_usu`, `facebook`, `instagram`, `twitter`, `img_usu`) VALUES
(76, 'V-21024060', 'carlos', 'castellanos', 'castellanoscarlos15@gmail.com', '04120545378', '12345', NULL, '2020-11-02 00:48:05', NULL, NULL, NULL, NULL, NULL),
(77, 'V-11886187', 'miguel', 'farias', 'miguelfarias1@gmail.com', '04122680712', 'migfar', NULL, '2020-11-03 08:49:29', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehiculos`
--

CREATE TABLE `vehiculos` (
  `id` int(11) NOT NULL,
  `id_modelo` int(11) DEFAULT NULL,
  `nom_veh` varchar(100) DEFAULT NULL,
  `ano_veh` int(11) DEFAULT NULL,
  `pre_veh` float DEFAULT NULL,
  `tra_veh` varchar(20) DEFAULT NULL,
  `est_veh` int(11) DEFAULT NULL,
  `imp_veh` varchar(1) DEFAULT NULL,
  `sto_veh` int(11) DEFAULT NULL,
  `des_veh` text,
  `fle_veh` text,
  `dis_veh` text,
  `seg_veh` text,
  `img1` text,
  `img2` text,
  `img3` text,
  `img4` text,
  `img5` text,
  `doc` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `vehiculos`
--

INSERT INTO `vehiculos` (`id`, `id_modelo`, `nom_veh`, `ano_veh`, `pre_veh`, `tra_veh`, `est_veh`, `imp_veh`, `sto_veh`, `des_veh`, `fle_veh`, `dis_veh`, `seg_veh`, `img1`, `img2`, `img3`, `img4`, `img5`, `doc`) VALUES
(5, 5, 'Yaris HB', 2019, 0, 'Manual', 1, '1', 0, 'DISEÑO AL SIGUIENTE NIVEL\r\n\r\nSu diseño va un paso más allá ofreciendo una presencia exterior distintiva, manteniendo la movilidad y agilidad necesaria para\r\nconducir con absoluta soltura.\r\n\r\nMAYOR AMPLITUD Y COMODIDAD\r\n\r\nCon su amplia carrocería, el Yaris Hatchback ofrece una comodidad excepcional. Su capacidad y amplitud son dos de las\r\ncaracterísticas más admirables de este modelo.', 'EFICIENCIA\r\n\r\nLa conducción del Yaris está por\r\nencima de su categoría, presentando\r\nestabilidad, seguridad y confort en la\r\ndirección. Equipado con dirección\r\nelectro-asistida, el vehículo tiene más\r\nfacilidad y suavidad en la conducción.\r\nSu motor 1.5L Dual VVT-i entrega\r\nrendimiento y eficiencia, apostando a\r\nla sincronía absoluta entre potencia y\r\nahorro de combustible', 'Las líneas modernas e imponentes\r\ntraen personalidad y deportividad\r\ncon la rejilla frontal tipo colmena.\r\nLos faros con un diseño\r\nsobresaliente tienen proyectores\r\nintegrados con máscara negra. En\r\ndefinitiva su diseño único hace del\r\nYaris un vehículo vanguardista y\r\nsofisticado.', 'MAYOR SEGURIDAD Y CONTROL\r\n\r\nConducir por la ciudad tiene sus riesgos y una carrocería más\r\nrígida proporciona mayor seguridad en caso de una colisión. Es\r\npor esto que hemos cambiado la forma en que las láminas de\r\nmetal se unen en la construcción de la carrocería. Además, para\r\nhacerlo aún más fuerte, aumentamos el número de puntos de\r\nsoldadura por 100.\r\n\r\nEquipado con airbags para conductor y\r\npasajero, el Yaris HB te proporciona\r\nseguridad. Además, está equipado con\r\ncontrol de estabilidad (VSC) y asistente de\r\nsubida (HAC) que proporciona comodidad\r\nen salidas con inclinación, impidiendo que\r\nel vehículo descienda por hasta tres\r\nsegundos para el cambio de pedal.', 'https://exiauto.com/static/img/files/2019_11_06_09_10_51yaris hb blanco frente.png', 'http://pladecompany.com/demos/exiauto.com.ve/static/img/files/2019_09_10_11_43_58yaris hb blanco lado.png', 'http://pladecompany.com/demos/exiauto.com.ve/static/img/files/2019_09_10_11_43_58yaris hb espalda blanco lado.png', 'http://pladecompany.com/demos/exiauto.com.ve/static/img/files/2019_09_10_11_43_58yaris hb espalda blanco.png', 'http://pladecompany.com/demos/exiauto.com.ve/static/img/files/2019_09_10_11_43_58yaris hb blanco frente.png', 'https://exiauto.com/static/img/files/2019_09_17_21_51_46formulario_solicitud_visa_DANIELA.pdf'),
(6, 5, 'Yaris SD', 2019, 0, 'Manual', 1, '1', 0, '+ ATRACTIVO + SEGURO + ESTABLE + CÓMODO', 'La conducción del Yaris está por\r\nencima de su categoría, presentando\r\nestabilidad, seguridad y confort en la\r\ndirección. Equipado con dirección\r\nelectro-asistida, el vehículo tiene más\r\nfacilidad y suavidad en la conducción.\r\nSu motor 1.5L Dual VVT-i entrega\r\nrendimiento y eficiencia, apostando a\r\nla sincronía absoluta entre potencia y\r\nahorro de combustible', 'Las líneas modernas e\r\nimponentes traen personalidad y\r\nsofisticación junto con la parrilla\r\nfrontal.\r\nLos faros con diseño destacado\r\nposeen proyectores integrados\r\ncon máscara negra.\r\n\r\nCon su amplia carrocería, el Yaris Sedan ofrece una comodidad excepcional. Su capacidad y amplitud son dos de\r\nlas características más admirables de este modelo.\r\nEl Yaris Sedan va un paso más lejos con un habitáculo atractivo y amplio, diseñado para brindar la máxima\r\ncomodidad y las mejores sensaciones a sus ocupantes.', 'MAYOR SEGURIDAD Y CONTROL\r\nConducir por la ciudad tiene sus riesgos y una carrocería más\r\nrígida proporciona mayor seguridad en caso de una colisión. Es\r\npor esto que hemos cambiado la forma en que las láminas de\r\nmetal se unen en la construcción de la carrocería. Además, para\r\nhacerlo aún más fuerte, aumentamos el número de puntos de\r\nsoldadura por 100.\r\n\r\nNew Yaris está equipado con la tecnología VSC la cual permite\r\nestabilizar el movimiento del vehículo, ayudando a que el\r\nconductor controle el derrape lateral.\r\nEquipado con hasta 7 airbags el Yaris Sedan\r\nte proporciona seguridad. Además, está\r\nequipado con control de estabilidad (VSC) y\r\nasistente de subida (HAC) que proporciona\r\ncomodidad en salidas con inclinación,\r\nimpidiendo que el vehículo descienda por\r\nhasta tres segundos para el cambio de\r\npedal.', 'http://pladecompany.com/demos/exiauto.com.ve/static/img/files/2019_09_10_12_39_46Picture2-min.png', 'http://pladecompany.com/demos/exiauto.com.ve/static/img/files/2019_09_10_12_39_46FRENTE LADO YARIS SUPER BLANCO.png', 'http://pladecompany.com/demos/exiauto.com.ve/static/img/files/2019_09_10_12_28_46FRENTE FRENTE YARIS BLANCO.png', 'http://pladecompany.com/demos/exiauto.com.ve/static/img/files/2019_09_10_12_39_46SUPER BLANCO YARIS ESPALDA LADO.png', 'http://pladecompany.com/demos/exiauto.com.ve/static/img/files/2019_09_10_12_28_46ESPALDA YARIS BLANCO.png', NULL),
(7, 6, 'Fortuner Importada', 2019, 0, 'Manual', 1, '1', 1, '', '', '', '', 'https://exiauto.com/static/img/files/2019_09_12_09_56_47frente fortuner importada-min.jpg', 'http://pladecompany.com/demos/exiauto.com.ve/static/img/files/2019_09_10_13_27_17faros delanteros fortuner importada.jpg', 'https://exiauto.com/static/img/files/2019_09_12_09_56_48interior puestos fortuner importada-min.jpg', 'https://exiauto.com/static/img/files/2019_09_12_09_56_48luces traseras fortuner importada-min.jpg', NULL, NULL),
(8, 7, 'Hiace Panel ', 2019, 0, 'Manual', 1, '1', 0, 'La Toyota Hiace diseñada para emprendedores que necesitan una máquina de carga perfecta para los objetivos de su negocio, sumamente versátil y de una alta funcionalidad.\r\n\r\n Déjate sorprender con la nueva Hiace, un espacio tan grande como tus sueños.', 'Toyota Hiace posee una excelente maniobrabilidad gracias a su reducido radio de giro (5.0 mts. para techo bajo, 6.2 mts. para techo alto).\r\n', '• Asientos delanteros reclinables tipo butaca con apoya cabeza \r\n• Faros de Halógeno \r\n• Tercer Stop (Tipo Led)\r\n• Múltiples compartimientos\r\n• Tapicería de tela Gris \r\n• Portavasos \r\n• Sistema de Audio AM/FM con entrada USB \r\n• Aire Acondicionado ', '• Cinturones de seguridad delanteros de 3 puntos (pasajero y co-piloto) \r\n• Cinturones de seguridad delantero de 2 puntos (central) \r\n• Estructura de carrocería rígida amortiguada contra impactos\r\n• Volante basculante y columna de dirección colapsable en caso de impacto\r\n• Barras de seguridad en las puertas', 'https://exiauto.com/static/img/files/2019_09_12_09_32_11hiace blanca cerrada frente-min.PNG', 'https://exiauto.com/static/img/files/2019_09_12_09_32_11hiace lado blanca cerrada}-min.PNG', 'https://exiauto.com/static/img/files/2019_09_12_09_32_11panel beige and brown dos puestos-min.PNG', 'https://exiauto.com/static/img/files/2019_09_12_09_32_11espalda desde afuera con vacia llena-min.PNG', 'https://exiauto.com/static/img/files/2019_09_12_09_32_12interior vacio dos puestos-min.PNG', NULL),
(9, 1, 'Corolla Nacional ', 2019, 0, 'Manual', 1, '0', 0, 'Corolla Más allá de la perfección\r\nLa experiencia de conducir un Toyota Corolla es inigualable, ya que posee el equilibrio perfecto entre potencia, maniobrabilidad y rendimiento, además de proveer un espacio cómodo, amplio, confortable para el conductor y toda su familia.\r\n\r\n\r\nPasión, innovación, elegancia, tecnología e imagen deportiva son elementos que marcan la creación del nuevo Toyota Corolla un vehículo nunca antes visto, una creación totalmente nueva producto de la ingeniería Toyota', '', 'Diseño vanguardista y dimensiones que proyectan mayor valor de mayor dimensiones:\r\nLargo: 4620mm (+80) / Ancho:1775 mm (+15) / Alto: 1475mm (-5) a mas largo y deportivo.\r\nDist. entre ejes: 2700mm (+100)= mayor espacio interior.\r\nParrilla incorporada a los faros = Elegancia y sensación de anchura.\r\nParrilla inferior en forma de trapezoide invertido (Deportivo).\r\nLíneas de corte más pronunciadas en los extremos de los parachoques (Guardafangos más anchos a estabilidad).\r\nLíneas de cortes pronunciadas en los laterales (largo y estabilidad).\r\nPlatina cromada que se entrelaza con los faros traseros = Presencia y elegancia.\r\nFaros tipo LED = Menor consumo, mayor iluminación y alcance.\r\nElementos aerodinámicos que ayudan a disminuir la fricción del viento y a la disminución del ruido.\r\nDiseño vanguardista y elegante con una fluidez horizontal (dinamismo).\r\nJuego de colores \"Claro - Oscuro\" (Amplitud y frescura).\r\nDist. entre ejes: 2700 (+100) = mayor espacio interior. Incrementa el espacio de rodillas en el asiento trasero en 75mm.\r\nMejora en los materiales simulando el cuero (Menos plástico y más nivel) con detalles de costura (Elegancia).\r\nAcabados en Piano Black y tipo metálicos (Lujo, Brillo y resistencia a las rayas).\r\nContinuidad de la parrilla en el faro.\r\nFaros tipo LED (GLi).\r\nNueva llave de encendido tipo navaja.\r\nVolante forrado en cuero con controles de audio y manos libres (GLi).\r\nConsola de techo (GLi).\r\nCámara de retroceso (GLi).\r\nAsiento con respaldar abatible (GLi).\r\nAmpliación del espacio en la maleta a pesar de tener cilindro de GNV.\r\nTres bolsas de aire (Conductor + Pasajero y Rodilla de conductor).\r\nNuevo radio de pantalla táctil.\r\nIpod, Teléfono, USB, SD Card, DVD, Bluetooth.\r\nFijación para sillas de bebé.', '', 'http://pladecompany.com/demos/exiauto.com.ve/static/img/files/2019_09_10_14_22_30FRENTE COROLLA.jpg', 'http://pladecompany.com/demos/exiauto.com.ve/static/img/files/2019_09_10_14_22_30LADO COROLLA.jpg', 'http://pladecompany.com/demos/exiauto.com.ve/static/img/files/2019_09_10_14_22_30LUCES COROLLA.jpg', 'http://pladecompany.com/demos/exiauto.com.ve/static/img/files/2019_09_10_14_22_30LUCES TRASERAS COROLLA.jpg', 'http://pladecompany.com/demos/exiauto.com.ve/static/img/files/2019_09_10_14_22_31MALETA COROLLA.jpg', NULL),
(10, 2, 'Camry', 2019, NULL, 'Automático', 1, '1', 0, 'Lujo y confort a simple vista\r\n', NULL, 'El nuevo Camry es la síntesis perfecta entre moderno, deportivo y elegante. El renovador frente realza su presencia a partir del diseño de las ópticas y de la parrilla inferior. Las llantas de 17” remarcan un perfil más deportivo y audaz.\r\n\r\nSu renovador interior ofrece asientos tapizados en cuero color negro y detalles similar a la madera. El audio con pantalla táctil de 8” y el display de información múltiple de 7” pueden controlarse desde el volante.', 'Además de tener una carrocería de seguridad para impactos con la clasificación global más alta, el Camry está repleto de funciones avanzadas en seguridad activa. Por si fuera poco, también está equipado con una serie de medidas de seguridad pasiva: airbags frontales, laterales, de cortina y de rodilla para proteger la integridad de los ocupantes ante la ocasión de un impacto.\r\n\r\nEl control de estabilidad (vsc) ayudará a mantener todo en orden ante situaciones adversas. El Camry cuenta con discos de 17” en las 4 ruedas, además de sistema de anti-bloqueo ABS y BA (BrakeAssist) que brindan la mayor seguridad y potencia de frenado.\r\n\r\n Airbags frontales, laterales, de cortina y de rodilla para conductor, para proteger la integridad de los ocupantes ante la ocasión de un impacto.', 'https://exiauto.com/static/img/files/2019_09_12_09_01_01camry calle.jpg', 'https://exiauto.com/static/img/files/2019_09_12_09_01_01camry frente.PNG', NULL, 'https://exiauto.com/static/img/files/2019_09_12_09_01_01espalda lado camry.PNG', 'https://exiauto.com/static/img/files/2019_09_12_09_01_02camry espalda.PNG', NULL),
(11, 7, 'Hiace Commuter', 2019, 0, 'Manual', 1, '1', 0, '', '', '', '', 'https://exiauto.com/static/img/files/2019_09_12_09_42_44hiace commuter frente-min.jpg', 'https://exiauto.com/static/img/files/2019_09_12_09_42_45hiace lado commuter-min.PNG', 'https://exiauto.com/static/img/files/2019_09_12_09_42_45hiace commuter espalda-min.PNG', 'https://exiauto.com/static/img/files/2019_09_12_09_42_45interior puestos-min.PNG', 'https://exiauto.com/static/img/files/2019_09_12_09_42_46panel 3 puestos-min.PNG', NULL),
(12, 6, 'Fortuner Nacional', 2019, 0, 'Manual', 1, '0', 0, '', '', '', '', 'https://exiauto.com/static/img/files/2019_09_12_09_49_54FRENTE FORTUNER NACIONAL-min.jpg', 'https://exiauto.com/static/img/files/2019_09_12_09_49_54LADO FORTUNER NACIONAL-min.jpg', 'https://exiauto.com/static/img/files/2019_09_12_09_49_55ESPALDA FORTUNER NACIONAL-min.jpg', 'https://exiauto.com/static/img/files/2019_09_12_09_49_55TABLERO FORTUNER-min.jpg', 'https://exiauto.com/static/img/files/2019_09_12_09_49_55ASIENTO FORTUNER-min.jpg', NULL),
(14, 10, 'Land Cruiser Prado VX', 2020, 0, 'Automático', 1, '1', 0, 'Con un aspecto fuerte y un alto nivel de calidad, durabilidad y confiabilidad, que alcanza los estándares más altos de diseño e innovación', 'Excelente maniobrabilidad', 'Diseño “modernidad inteligente” que da un carácter elegante y robusto, espacio interior más amplio', 'El radar detecta los vehículos que circulan en el siguiente carril. Cuando el vehículo ingresa al área del punto ciego, el indicador LED montado en el espejo de la puerta se enciende. En ese momento, si la luz de giro lateral parpadea, el indicador LED también parpadea para alertar al conductor.', 'https://exiauto.com/static/img/files/2019_10_02_11_24_39pradoprado.jpg', NULL, NULL, NULL, NULL, 'https://exiauto.com/static/img/files/2019_10_02_11_26_20Spec Sheet PRADO.pdf'),
(15, 6, 'Fortuner TRD Sportivo', 2020, 0, 'Automático', 1, '0', 0, 'La nueva Fortuner en sus dos versiones, 4x4 y 4x2, presenta la combinación perfecta entre elegancia, confort y un alto rendimiento capaz de operar con tranquilidad en condiciones extremas. La armonía del nuevo diseño presenta líneas redondeadas que proporcionan una sensación de fuerza y robustez con detalles deportivos, sin perder la elegancia y sofisticación de un vehículo de categoría superior.', 'Aire acondicionado automático.\r\nSistema de climatización automática digital.\r\n\r\n\r\nAire acondicionado trasero.\r\nSistema de enfriamiento trasero independiente para mayor confort en las dos filas de asientos posteriores.\r\n\r\n\r\nNumerosos espacios de almacenamiento.\r\nFortuner tiene diversos espacios de almacenamiento para colocar artículos que se deseen tener a la mano.\r\n\r\n\r\nConsola de techo con luces de lectura.\r\nLa consola superior entre los dos tapasoles está perfectamente adecuada para guardar los lentes de sol u otros artículos de tamaño similar. Igualmente posee doble lámparas de lectura para mayor confort del cliente.', 'Tablero central y paneles.\r\nDetalles internos cromados y apliques tipo madera oscura.\r\n\r\n\r\nInnovador panel de instrumento optitrón.\r\nEl panel de instrumentos tipo optitron permite al conductor tener una fácil lectura de cada uno de los componentes del tablero, permitiendo una conducción más segura.\r\n\r\n\r\nVolantes con controles de audio.\r\nLos controles de audio ubicados en el volante le permiten ajustar las diferentes funciones del sistema de audio (volúmen, estaciones, canciones y modo de reproducción) sin perder la visión en el camino, permitiendo su manejo seguro.\r\n\r\n\r\nTapicería de cuero.\r\n', 'Sistema inmobilizer\r\nPara mayor seguridad, la Fortuner está equipada con un sistema que bloquea el encendido.\r\n\r\n\r\nABS\r\nSistema de frenos antibloqueo-ABS\r\n\r\n\r\nDoble bolsa de aire\r\nDoble bolsa de aire para conductor y pasajero.\r\n\r\n\r\nCarrocería de seguridad\r\nLa carrocería de seguridad contra choques abarca un habitáculo de alta integridad, con zonas de absorción de impacto delantera y traseras, que minimizan la transmisión del impacto hacia los pasajeros.\r\n\r\n\r\nAlarma antirrobo.\r\nLa Fortuner tiene un sistema de seguridad que avisa en caso de un robo o el intento del mismo.', 'https://exiauto.com/static/img/files/2019_10_17_14_10_18fortuner trd sportivo.jpg', NULL, NULL, NULL, NULL, NULL),
(16, 8, 'Hilux Gr Sport', 2020, 0, 'Automático', 1, '1', 0, 'Todo lo que aprendimos de las competencias más exigentes llevado a una Pick-Up', 'Robusta y deportiva, con una parrilla imponente que destaca el emblema Toyota, barra y estribos de diseño exclusivo y llantas de aleación específicas.', 'Destacan los colores negro y rojo. Las butacas son de cuero natural ecológico, y todas vienen con un número único que las identifica. La nueva GR-Sport respira las carreras incluso desde el botón de arranque. ', 'Todas las versiones vienen equipadas en la parte delantera con una cámara frontal Dashcam que registra tus momentos en Full HD, y viene con GPS y WiFi incorporado.', 'https://exiauto.com/static/img/files/2020_02_05_09_14_11gr sport-3.jpg', 'https://exiauto.com/static/img/files/2020_02_05_09_14_11gr sport-4.jpg', 'https://exiauto.com/static/img/files/2020_02_05_09_14_11gr sport-5.jpg', 'https://exiauto.com/static/img/files/2020_02_05_09_14_11gr sport-6.jpg', 'https://exiauto.com/static/img/files/2020_02_05_09_14_12gr sport-55.jpg', 'https://exiauto.com/static/img/files/2020_02_05_09_14_12Hilux GR Sport - Specs.pdf');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `usuario` (`usuario`),
  ADD UNIQUE KEY `correo` (`correo`);

--
-- Indices de la tabla `agentes`
--
ALTER TABLE `agentes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cod_age` (`cod_age`);

--
-- Indices de la tabla `categorias_repuestos`
--
ALTER TABLE `categorias_repuestos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `citas`
--
ALTER TABLE `citas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_usuario` (`id_usuario`),
  ADD KEY `id_vehiculo` (`id_vehiculo`),
  ADD KEY `id_agente` (`id_agente`),
  ADD KEY `id_falla` (`id_falla`),
  ADD KEY `id_kilometros` (`id_kilometros`);

--
-- Indices de la tabla `comentarios_reclamos`
--
ALTER TABLE `comentarios_reclamos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_usuario` (`id_usuario`),
  ADD KEY `id_admin` (`id_admin`),
  ADD KEY `id_reclamo` (`id_reclamo`);

--
-- Indices de la tabla `departamentos`
--
ALTER TABLE `departamentos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nom_dep` (`nom_dep`);

--
-- Indices de la tabla `fallas`
--
ALTER TABLE `fallas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `kilometrajes`
--
ALTER TABLE `kilometrajes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `misvehiculos`
--
ALTER TABLE `misvehiculos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `placa` (`placa`),
  ADD KEY `id_usuario` (`id_usuario`),
  ADD KEY `misvehiculos_ibfk_2` (`id_modelo`);

--
-- Indices de la tabla `modelos`
--
ALTER TABLE `modelos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `modelos_citas`
--
ALTER TABLE `modelos_citas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `reclamos`
--
ALTER TABLE `reclamos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_usuario` (`id_usuario`),
  ADD KEY `id_departamento` (`id_departamento`);

--
-- Indices de la tabla `repuestos`
--
ALTER TABLE `repuestos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_categoria` (`id_categoria`);

--
-- Indices de la tabla `solicitud_repuestos`
--
ALTER TABLE `solicitud_repuestos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ced_usu` (`ced_usu`),
  ADD UNIQUE KEY `cor_usu` (`cor_usu`);

--
-- Indices de la tabla `vehiculos`
--
ALTER TABLE `vehiculos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_modelo` (`id_modelo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `agentes`
--
ALTER TABLE `agentes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `categorias_repuestos`
--
ALTER TABLE `categorias_repuestos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `citas`
--
ALTER TABLE `citas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT de la tabla `comentarios_reclamos`
--
ALTER TABLE `comentarios_reclamos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `departamentos`
--
ALTER TABLE `departamentos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `fallas`
--
ALTER TABLE `fallas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=202;

--
-- AUTO_INCREMENT de la tabla `kilometrajes`
--
ALTER TABLE `kilometrajes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=231;

--
-- AUTO_INCREMENT de la tabla `misvehiculos`
--
ALTER TABLE `misvehiculos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT de la tabla `modelos`
--
ALTER TABLE `modelos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `modelos_citas`
--
ALTER TABLE `modelos_citas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `noticias`
--
ALTER TABLE `noticias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `reclamos`
--
ALTER TABLE `reclamos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `repuestos`
--
ALTER TABLE `repuestos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `solicitud_repuestos`
--
ALTER TABLE `solicitud_repuestos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT de la tabla `vehiculos`
--
ALTER TABLE `vehiculos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `citas`
--
ALTER TABLE `citas`
  ADD CONSTRAINT `citas_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`),
  ADD CONSTRAINT `citas_ibfk_2` FOREIGN KEY (`id_vehiculo`) REFERENCES `misvehiculos` (`id`),
  ADD CONSTRAINT `citas_ibfk_3` FOREIGN KEY (`id_agente`) REFERENCES `agentes` (`id`),
  ADD CONSTRAINT `citas_ibfk_4` FOREIGN KEY (`id_falla`) REFERENCES `fallas` (`id`),
  ADD CONSTRAINT `citas_ibfk_5` FOREIGN KEY (`id_kilometros`) REFERENCES `kilometrajes` (`id`);

--
-- Filtros para la tabla `comentarios_reclamos`
--
ALTER TABLE `comentarios_reclamos`
  ADD CONSTRAINT `comentarios_reclamos_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`),
  ADD CONSTRAINT `comentarios_reclamos_ibfk_2` FOREIGN KEY (`id_admin`) REFERENCES `admins` (`id`),
  ADD CONSTRAINT `comentarios_reclamos_ibfk_3` FOREIGN KEY (`id_reclamo`) REFERENCES `reclamos` (`id`);

--
-- Filtros para la tabla `misvehiculos`
--
ALTER TABLE `misvehiculos`
  ADD CONSTRAINT `misvehiculos_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`),
  ADD CONSTRAINT `misvehiculos_ibfk_2` FOREIGN KEY (`id_modelo`) REFERENCES `modelos_citas` (`id`);

--
-- Filtros para la tabla `reclamos`
--
ALTER TABLE `reclamos`
  ADD CONSTRAINT `reclamos_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`),
  ADD CONSTRAINT `reclamos_ibfk_2` FOREIGN KEY (`id_departamento`) REFERENCES `departamentos` (`id`);

--
-- Filtros para la tabla `repuestos`
--
ALTER TABLE `repuestos`
  ADD CONSTRAINT `repuestos_ibfk_1` FOREIGN KEY (`id_categoria`) REFERENCES `categorias_repuestos` (`id`);

--
-- Filtros para la tabla `vehiculos`
--
ALTER TABLE `vehiculos`
  ADD CONSTRAINT `vehiculos_ibfk_1` FOREIGN KEY (`id_modelo`) REFERENCES `modelos` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
