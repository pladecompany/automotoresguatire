<?php

  include_once("panel-admin-toyota/modelo/Repuesto.php");  
  include_once("panel-admin-toyota/modelo/Categoria.php");  
  include_once("panel-admin-toyota/modelo/ModeloRepuesto.php");  
  
  $repuesto = new Repuesto();
  $categoria = new Categoria();
  $modeloR = new ModeloRepuesto();

  if(isset($_GET['modelo_ano']) && isset($_GET['ano'])){
	$idm = $_GET['modelo_ano'];
	$ano = $_GET['ano'];

	$rep = $modeloR->findById($idm);
	$resultados = $repuesto->getRepuestosByAno($idm, $ano);
	$titulo = "Repuestos pertenecientes al modelo '".$rep['modelo']."' y el año '".$ano."', resultados (" .$resultados->num_rows .")";

  }else if(isset($_GET['categoria'])){
	$idc = $_GET['categoria'];
	$rep = $categoria->findById($idc);
	$resultados = $repuesto->getRepuestosBYCategoria($idc);
	$titulo = "Repuestos pertenecientes a la categoría '".$rep['nom_cat']."', resultados (" .$resultados->num_rows .")";
  }else if(isset($_GET['modelo'])&&!isset($_GET['txt'])){
	$idm = $_GET['modelo'];
	$rep = $modeloR->findById($idm);
	$resultados = $repuesto->getRepuestosBYModelo($idm);
	$titulo = "Repuestos pertenecientes al modelo  '".$rep['modelo']."', resultados (" .$resultados->num_rows .")";
  }else if(isset($_GET['txt'])&&isset($_GET['modelo'])){
	$txt = $_GET['txt'];
	$idm = $_GET['modelo'];
	$rep = $modeloR->findById($idm);
	$resultados = $repuesto->getRepuestosBYNombreModelo($txt, $idm);
	$titulo = "Repuestos de la busqueda '".$txt."' en el modelo '".$rep['modelo']."', resultados (" .$resultados->num_rows .")";
  }else if(isset($_GET['txt'])){
	$txt = $_GET['txt'];
	$resultados = $repuesto->getRepuestosBYNombre($txt);
	$titulo = "Repuestos de la busqueda '".$txt."', resultados (" .$resultados->num_rows .")";
  }else{
	$titulo = "Ultimos repuestos publicados";

	$resultados = $repuesto->getUltimosPublicados(10);
  }
  
?>
<section id="vehiculos" class="section-repuestos no-padding">
	<div class="row m-0 pb-5" style="background: #EEE;padding: 1rem;">
		<div class="col-md-12 text-center">
			<h2 class="mb-4">Repuestos</h2>
			<div class="container">
				<div class="row">
					<form method="GET" class="row" style="width:100%;">
						<input type="hidden" name="op" value="repuestos">
						<?php
							if(isset($_GET['modelo'])){
						?>
							<input type="hidden" name="modelo" value="<?php echo $_GET['modelo'];?>">
						<?php
						}
						?>
						<div class="col-md-2"></div>
						<div class="col-md-6">
							<div class="form-group">
								<input name="txt" type="search" class="form-control" placeholder="" required style="height: auto !important;">
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group text-left">
								<button type="submit" name="bt_buscar" class="btn btn-primary"><i class="fa fa-search"></i> Buscar</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<br>

	<div class="mt-5 px-md-5 container">
		<div class="row">
			<div class="col-md-3">
				<ul class="list-group list-group-flush">
					<?php
						$modelos = $repuesto->getModelosRepuestos();
						while($mode = $modelos->fetch_assoc()){
						$anos = $repuesto->getAnosModelo($mode['id']);
						sort($anos); 
						echo "<li class='list-group-item mostrar_anos' id='".$mode['id']."'><a href='?op=repuestos&modelo=".$mode['id']."'>".$mode['modelo']."</a>";
						if(count($anos)>0){
							echo "<a class='pull-right' style='cursor:pointer;font-size:10px;'><b>Años</b> <i class='fa fa-arrow-down'></i></a>";
							echo "<br>";
							echo '<ul class="list-group list-group-flush" id="anos_'.$mode['id'].'" style="text-transform:uppercase;display:none;">';
							for($i=0;$i<count($anos);$i++)
							echo "<li class='list-group-item text-right' style='font-size:10px;'><a href='?op=repuestos&modelo_ano=".$mode['id']."&ano=".$anos[$i]."'>".$anos[$i]."</a>";
							echo '</ul>';
						}
						echo "</li>";
						}

					?>
				</ul>
				<br>

				<div class="card-header">
					Categorías
				</div>
				<ul class="list-group list-group-flush" style='text-transform:uppercase;'>
					<?php
						$categorias = $categoria->fetchAll();
						while($cate = $categorias->fetch_assoc()){
						echo "<li class='list-group-item'><a href='?op=repuestos&categoria=".$cate['id']."'>".$cate['nom_cat']."</a></li>";
					}
					?>
				</ul>
			</div>

			<div class="col-md-9">
				<div class="row">
					<div class="col-md-12">
						<h6><?php echo $titulo; ?></h6>
						<hr>
					</div>
				</div>

				<div class="row">
					<?php
					while($data = $resultados->fetch_assoc()){
					?>
					<div class="col-md-4 mb-3">
						<a href="?op=repuesto-ver&id=<?php echo $data['id'];?>">
							<div class="card">
								<div class="text-center">
									<img class="card-img-top img-repuestos" src="<?php echo $data['img1'];?>" alt="">
								</div>

								<div class="card-body p-3">
									<h5 class="card-title"><?php echo $data['nom_rep'];?></h5>
									<!--<b class="" style="color:#000;">CÓDIGO: <?php // echo $data['cod_rep'];?></b>-->
									<p class="card-text"><?php echo substr($data['des_rep'], 0, 30);?>. . .</p>
									<h3 class="card-text"><b>U$S <?php echo substr($data['pre_rep'], 0, 30);?></b></h3>
								</div>

								<div class="card-footer p-3">
									<a href="?op=repuesto-ver&id=<?php echo $data['id'];?>" target="_blank" class="btn btn-primary">Me interesa</a>
								</div>
							</div>
						</a>
						<br>
					</div>

					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>

<script>
	$(document).ready(function(){
		$(document).on('click', '.mostrar_anos', function(){
			var id = this.id;
			if($("#anos_"+id).is(":visible"))
				$("#anos_"+id).hide();
			else
			$("#anos_"+id).show();
		});
	});
</script>
