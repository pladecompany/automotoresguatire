<?php
	include_once("panel/modelo/MiVehiculo.php"); 
	include_once("panel/controlador/solicitudes.php"); 
?>
<br><br>
<div class="container">
	<div class="card shadow mb-4 ml-5 mb-4 mr-5 mt-4">
		<div class="card-header py-3">
			<h4 class="m-0 font-weight-bold color-b">Mis citas</h4>
			
			<div class="text-right mb-4">
				<a href="#md-slider" data-toggle="modal" class="color-b modal-trigger btn-primary" id="bt_nueva_noticia"><b><i class="fa fa-plus-circle"></i> Nueva cita </b></a>
			</div>
		</div>

		<div class="card-body">
			<?php include_once("mensajes.php");?>
			<div class="table-responsive">
				<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>#</th>
							<th>Vehículo</th>
							<th>Fecha de cita</th>
							<th>Descripción</th>
							<th>Departamento</th>
							<!--<th>Estatus</th>-->
						</tr>
					</thead>

					<tbody>
						<?php
		                $noti = new Solicitudes();
		                $r = $noti->fetchAll($_SESSION['idu']);
		                $i=0;
		                while($ff = $r->fetch_assoc()){
		                  $i++;
		                  echo "<tr>";
		                  echo "  <td>" . $i . "</td>";
		                  echo "  <td>" . $ff['placa'] . " / " . $ff['modelo'] . "</td>";
		                  echo "  <td>" . $ff['fec_sol'] . "</td>";
		                  echo "  <td>" . $ff['descrip'] . "</td>";
		                  echo "  <td>" . $ff['depar'] . "</td>";
		                  echo "</tr>";
		                }
		              ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div id="md-slider" class="modal modalmedium fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">

			<div class="modal-header">
            <h6 class="title-d" id="titulo_modulo">Nueva cita</h6>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form class="form-a" action="" id="formulario_registro_cliente" method="POST">
				<div class="modal-body">
					<div clas="col-md-12 mb-2 text-center" style="margin-bottom:0px !important;display:none;text-align:center !important;" id="cont_msj">
						<span id="txt_msj" style="color:red;text-align:center;"></span>
						<br><br>
					</div>

					<div class="row">
						<div class="col-md-6 mb-2" style="margin-bottom:0px !important;">
							<label for="ced">Vehículo</label>
							<select class="form-control form-control-lg form-control-a" name="cod1" required>
								<option selected="">--Seleccione su vehículo--</option>
								<?php
					                $noti = new MiVehiculo();
					                $r = $noti->misVehiculos($_SESSION['idu']);
					                while($ff = $r->fetch_assoc()){
					                  echo "<option value='".$ff['idv']."''>" . $ff['modelo'] . " / " . $ff['placa'] . "</option>";
					                }
					              ?>
							</select>
						</div>

						<div class="col-md-6 mb-2" style="margin-bottom:0px !important;">
							<div class="form-group">
								<label for="Fecha de cita">Fecha tentativa de su cita</label>
								<input type="date" class="form-control form-control-lg form-control-a" required name="fec" >
							</div>
						</div>

						<div class="col-md-12 mb-2" style="margin-bottom:0px !important;">
							<label for="ced">Departamento donde será atendido</label>
							<select class="form-control form-control-lg form-control-a" name="dep" required>
								<option selected>--Servicio--</option>
							</select>
						</div>

						<div class="col-md-12 mb-2 text-center" style="margin-bottom:0px !important;">
							<div class="form-group">
								<label for="Contraseña">&nbsp;</label>
								<textarea name="descrip" class="form-control form-control-lg form-control-a" placeholder="Descripción de su cita"></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
	                <button type="submit" id="bt_modulo" name="<?php echo ((isset($F))?'btc':'btg')?>" class="btn btn-primary"><?php echo ((isset($F))?'Guardar Cambios':'Enviar solicitud')?></button>
	            </div>
            </form>
		</div>
	</div>
</div>

<br><br>
