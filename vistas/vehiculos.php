<div id="banner-area" class="banner-area" style="background-image:url(static/img/banner/banner-vehiculos.jpg)">
	<div class="banner-text">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="banner-heading">
						<h1 class="banner-title">Vehículos</h1>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<section id="main-container" class="main-container">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<h3 class="column-title">Vehículos TOYOTA</h3>
				<p>En Automotores Guatire vendemos el line up nacional o importado producido o autorizado por Toyota de Venezuela para comercializar en el país, con garantía de 2 años o 50.000 km.</p>
				<blockquote><p>Te asesoramos para la mejor opción de compra, y otorgamos el respaldo de Toyota y el concesionario para que la compra de tu Toyota sea la mejor experiencia posible. </p></blockquote>
				<p>Desde que compras un carro con nosotros queremos que seas parte de nuestra familia.</p>
			</div>

			<div class="col-md-6">
				<div id="page-slider" class="owl-carousel owl-theme page-slider small-bg">
					<div class="item" style="background-image:url(static/img/slider-pages/slide-vehiculos-1.jpg)">
						<div class="container">
							<div class="box-slider-content">
								<div class="box-slider-text">
									<h2 class="box-slide-title">Vehículos TOYOTA</h2>
								</div>    
							</div>
						</div>
					</div>

				<div class="item" style="background-image:url(static/img/slider-pages/slide-vehiculos-2.jpg)">
					<div class="container">
						<div class="box-slider-content">
							<div class="box-slider-text">
								<h2 class="box-slide-title">Vehículos TOYOTA</h2>
							</div>
						</div>
					</div>
				</div>

				<div class="item" style="background-image:url(static/img/slider-pages/slide.jpg)">
					<div class="container">
						<div class="box-slider-content">
							<div class="box-slider-text">
								<h2 class="box-slide-title">Automotores Guatire</h2>
							</div>    
						</div>
					</div>
				</div>

				<div class="item" style="background-image:url(static/img/slider-pages/slide-2.jpg)">
					<div class="container">
						<div class="box-slider-content">
							<div class="box-slider-text">
								<h2 class="box-slide-title">Automotores Guatire</h2>
							</div>    
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>