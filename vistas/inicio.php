<div id="main-slide" class="carousel slide" data-ride="carousel">
	<ol class="carousel-indicators visible-lg visible-md">
		<li data-target="#main-slide" data-slide-to="0" class="active"></li>
		<li data-target="#main-slide" data-slide-to="1"></li>
		<li data-target="#main-slide" data-slide-to="2"></li>
	</ol>

	<div class="carousel-inner">
		<div class="item active" style="background-image:url(static/img/slider-main/bg1.jpg)">
			<div class="slider-content flexCenter">
				<div class="col-md-12 text-center">
					<h2 class="slide-title animated4">42 años de experiencia </h2>
					<h3 class="slide-sub-title animated5">Automotores Guatire</h3>
					<p>
						<a href="?op=servicio" class="slider btn btn-primary">Servicio</a>
						<a href="#contact" class="slider btn btn-primary border">Contáctanos ahora</a>
					</p>
				</div>
			</div>
		</div>

		<div class="item" style="background-image:url(static/img/slider-main/bg2.jpg)">
			<div class="slider-content flexCenter">
				<div class="col-md-12 text-center">
					<h2 class="slide-title animated6">Nacional y/o importados</h2>
					<h3 class="slide-sub-title animated7">Vehículos</h3>
					<p class="slider-description lead animated7"></p>
					<p>
						<a href="?op=vehiculos" class="slider btn btn-primary">Ver más</a>
						<a href="#contact" class="slider btn btn-primary border">Contáctanos</a>
					</p>
				</div>
			</div>
		</div>

		<div class="item" style="background-image:url(static/img/slider-main/bg3.jpg)">
			<div class="slider-content flexCenter">
				<div class="col-md-12 text-center">
					<h2 class="slide-title-box animated2">TOYOTA</h2>
					<h3 class="slide-title animated3">Todo original para tu Auto</h3>
					<h3 class="slide-sub-title animated3">Repuestos</h3>
					<p class="animated3">
						<a href="?op=repuestos" class="slider btn btn-primary border">Saber más</a>
					</p>
				</div>
			</div>
		</div>
	</div>

	<a class="left carousel-control" href="#main-slide" data-slide="prev">
		<span><i class="fa fa-angle-left"></i></span>
	</a>
	<a class="right carousel-control" href="#main-slide" data-slide="next">
		<span><i class="fa fa-angle-right"></i></span>
	</a>
</div>


<section class="call-to-action-box no-padding">
	<div class="container">
		<div class="action-style-box">
			<div class="row">
				<div class="col-md-10">
					<div class="call-to-action-text">
						<h3 class="action-title">Sabemos que necesitas de nuestro servicio</h3>
					</div>
				</div>

				<div class="col-md-2">
					<div class="call-to-action-btn">
						<a class="btn btn-dark" href="#contacto">Escríbenos</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="ts-features" class="ts-features">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-xs-12">
				<div class="ts-intro">
					<h3 class="into-sub-title">Tu concesionario TOYOTA</h3>
					<p>Somos tu concesionario de confianza <b>Automotores Guatire</b>, trabajando desde el año 1.978 ofreciendo vehículos nacionales y/o importados, repuestos originales y un servicio de calidad.</p>
				</div>

				<div class="gap-20"></div>

				<div class="row">
					<div class="col-md-6">
						<div class="ts-service-box">
							<span class="ts-service-icon">
								<i class="fa fa-trophy"></i>
							</span>
							<div class="ts-service-box-content">
								<h3 class="service-box-title">Excelente reputación</h3>
							</div>
						</div>
					</div>

					<div class="col-md-6">
						<div class="ts-service-box">
							<span class="ts-service-icon">
								<i class="fa fa-car"></i>
							</span>
							<div class="ts-service-box-content">
								<h3 class="service-box-title">Servicio de calidad</h3>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="ts-service-box">
							<span class="ts-service-icon">
								<i class="fa fa-thumbs-up"></i>
							</span>
							<div class="ts-service-box-content">
								<h3 class="service-box-title">Comprometidos con el trabajo</h3>
							</div>
						</div>
					</div>

					<div class="col-md-6">
						<div class="ts-service-box">
							<span class="ts-service-icon">
								<i class="fa fa-users"></i>
							</span>
							<div class="ts-service-box-content">
								<h3 class="service-box-title">Grupo de profesionales</h3>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-6 col-xs-12">
				<h3 class="into-sub-title">Acerca de nosotros</h3>
				<p><b>Automotores Guatire</b> estamos desde 1978 como concesionario Toyota</p>
				<div class="panel-group" id="accordion">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><i class="fas fa-angle-down"></i> Misión</a>
							</h4>
						</div>
						<div id="collapseOne" class="panel-collapse collapse in">
							<div class="panel-body">
								<p></p>
							</div>
						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapseTwo"><i class="fas fa-angle-down"></i> Visión</a>
							</h4>
						</div>
						<div id="collapseTwo" class="panel-collapse collapse">
							<div class="panel-body">
								<p></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="facts" class="facts-area dark-bg">
	<div class="container">
		<div class="row">
			<div class="facts-wrapper">
				<div class="col-sm-12 col-md-9 ts-facts">
					<div class="ts-facts-img">
						<h3 class=" m-0">Nuestros años de experiencia en el mercado</h3>
					</div>
				</div>

				<div class="col-sm-12 col-md-3 ts-facts">
					<div class="ts-facts-content">
						<h2 class="ts-facts-num m-0"><span class="counterUp" data-count="42">0</span></h2>
						<h3 class="ts-facts-title">AÑOS</h3>
					</div>
				</div>

	<!--			<div class="col-sm-4 ts-facts">
					<div class="ts-facts-img">
						<i class="fa fa-thumbs-up fa-3x clr_white"></i>
					</div>
					<div class="ts-facts-content">
						<h2 class="ts-facts-num"><span class="counterUp" data-count="647">0</span></h2>
						<h3 class="ts-facts-title">COMPROMETIDOS CON EL TRABAJO</h3>
					</div>
				</div>

				<div class="col-sm-4 ts-facts">
					<div class="ts-facts-img">
						<i class="fa fa-users fa-3x clr_white"></i>
					</div>
					<div class="ts-facts-content">
						<h2 class="ts-facts-num"><span class="counterUp" data-count="44">0</span></h2>
						<h3 class="ts-facts-title">PERSONAL</h3>
					</div>
				</div>
			-->
			</div>
		</div>
	</div>
</section>

<section id="ts-service-area" class="ts-service-area">
	<div class="container">
		<div class="row text-center">
			<h2 class="section-title">Especialistas en </h2>
			<h3 class="section-sub-title">Servicio TOYOTA</h3>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="ts-service-box">
					<div class="ts-service-box-img pull-left">
						<i class="fa fa-building fa-3x clr_red"></i>
					</div>
					<div class="ts-service-box-info">
						<h3 class="service-box-title"><a href="#">Instalaciones</a></h3>
						<p>Contamos con amplias instalaciones para nuestros clientes</p>
					</div>
				</div>

				<div class="ts-service-box">
					<div class="ts-service-box-img pull-left">
						<i class="fa fa-wrench fa-3x clr_red"></i>
					</div>
					<div class="ts-service-box-info">
						<h3 class="service-box-title"><a href="#">Herramientas Actualizada</a></h3>
						<p>Usamos herramientas especiales y la tecnología hacen que tengamos un servicio de calidad.</p>
					</div>
				</div>
			</div>

			<div class="col-md-4 text-center">
				<img class="service-center-img img-responsive" src="static/img/servicio.jpg" alt="" />
			</div>

			<div class="col-md-4">
				<div class="ts-service-box">
					<div class="ts-service-box-img pull-left">
						<i class="fa fa-car fa-3x clr_red"></i>
					</div>
					<div class="ts-service-box-info">
						<h3 class="service-box-title"><a href="#">Servicio especializado Toyota </a></h3>
						<p>Desde un simple cambio de aceite y filtro hasta la reconstrucción de motores.</p>
					</div>
				</div>

				<div class="ts-service-box">
					<div class="ts-service-box-img pull-left">
						<i class="fas fa-users-cog fa-3x clr_red"></i>
					</div>
					<div class="ts-service-box-info">
						<h3 class="service-box-title"><a href="#">Técnicos especializados</a></h3>
						<p>Tenemos técnicos capacitado en Toyota de Venezuela</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="subscribe no-padding">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-12 col-xs-12">
				<div class="subscribe-call-to-acton">
					<h3>¿Necesitas un repuesto? </h3>
					<h4><i class="fab fa-whatsapp"></i> (+58) 412 9009573 </h4>
				</div>
			</div><!-- Col end -->

			<div class="col-md-8 col-sm-12 col-xs-12">
				<div class="ts-newsletter">
					<div class="newsletter-introtext">
						<a href="#contacto" class="clr_white"><h4>Envíanos un correo</h4></a>
						<p>¡Te atenderemos!</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="content">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="row text-center">
					<h2 class="section-title">Clientes</h2>
					<h3 class="section-sub-title">Testimonios</h3>
				</div>

					<div class="col-md-3"></div>
					<div class="col-sm-12 col-md-6 text-center">
						<div id="testimonial-slide" class="owl-carousel owl-theme testimonial-slide">

						<div class="item">
							<div class="quote-item">
								<div class="quote-item-footer row">
									<div class="col-2 col-md-2 text-right">
										<img class="testimonial-thumb" src="static/img/testimonios/user.png" alt="testimonio">
									</div>

									<div class="col-10 col-md-8 quote-item-info text-left">
										<h3 class="quote-author">Richard Lopez</h3>
										<span class="quote-text"><i class="fa fa-quote-left"></i> Muy bueno</span>
									</div>
								</div>
							</div>
						</div>

						<div class="item">
							<div class="quote-item">
								<div class="quote-item-footer row">
									<div class="col-2 col-md-2 text-right">
										<img class="testimonial-thumb" src="static/img/testimonios/user.png" alt="testimonio">
									</div>

									<div class="col-10 col-md-8 quote-item-info text-left">
										<h3 class="quote-author">Johan Linares</h3>
										<span class="quote-text">
											Excelente equipo de trabajo y buen trato.. Los felicito.
										</span>
									</div>
								</div>
							</div>
						</div>
						
						<div class="item">
							<div class="quote-item">
								<div class="quote-item-footer row">
									<div class="col-2 col-md-2 text-right">
										<img class="testimonial-thumb" src="static/img/testimonios/user.png" alt="testimonio">
									</div>

									<div class="col-10 col-md-8 quote-item-info text-left">
										<h3 class="quote-author">Victor DOnofrio</h3>
										<span class="quote-text">
											Excelente Atención
										</span>
									</div>
								</div>
							</div>
						</div>
						
						<div class="item">
							<div class="quote-item">
								<div class="quote-item-footer row">
									<div class="col-2 col-md-2 text-right">
										<img class="testimonial-thumb" src="static/img/testimonios/user.png" alt="testimonio">
									</div>

									<div class="col-10 col-md-8 quote-item-info text-left">
										<h3 class="quote-author">Angel Subero</h3>
										<span class="quote-text">
											Excelente concesionario
										</span>
									</div>
								</div>
							</div>
						</div>
						
						<div class="item">
							<div class="quote-item">
								<div class="quote-item-footer row">
									<div class="col-2 col-md-2 text-right">
										<img class="testimonial-thumb" src="static/img/testimonios/user.png" alt="testimonio">
									</div>

									<div class="col-10 col-md-6 quote-item-info text-left">
								<h3 class="quote-author">Hernan Vallejo</h3>
								<span class="quote-text">
									Excelente
								</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!--
<section id="news" class="news">
	<div class="container">
		<div class="row text-center">
			<h2 class="section-title">Nacionales e importados</h2>
			<h3 class="section-sub-title">Vehículos</h3>
		</div>

		<div class="row">
			<div class="col-md-4 col-xs-12">
				<div class="latest-post">
					<div class="latest-post-media">
						<a href="#" class="latest-post-img">
							<img class="img-responsive" src="static/img/XPWU8769.JPG" alt="img">
						</a>
					</div>

					<div class="post-body">
						<h4 class="post-title">
							<a href="#">Vehículos</a>
						</h4>
						<div class="latest-post-meta">
							<span class="post-item-date">
								<i class="fa fa-car"></i> Nacional
							</span>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-4 col-xs-12">
				<div class="latest-post">
					<div class="latest-post-media">
						<a href="#" class="latest-post-img">
							<img class="img-responsive" src="static/img/XPWU8769.JPG" alt="img">
						</a>
					</div>
					<div class="post-body">
						<h4 class="post-title">
							<a href="#">Vehículos</a>
						</h4>
						<div class="latest-post-meta">
							<span class="post-item-date">
								<i class="fa fa-car"></i> Nacional
							</span>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-4 col-xs-12">
				<div class="latest-post">
					<div class="latest-post-media">
						<a href="#" class="latest-post-img">
							<img class="img-responsive" src="static/img/XPWU8769.JPG" alt="img">
						</a>
					</div>
					<div class="post-body">
						<h4 class="post-title">
							<a href="#">Vehículos</a>
						</h4>
						<div class="latest-post-meta">
							<span class="post-item-date">
								<i class="fa fa-car"></i> Importado
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="general-btn text-center">
			<a class="btn btn-primary" href="?op=vehiculos">Ver todos</a>
		</div>
	</div>
</section>
-->

<?php include("contacto.php"); ?>

<!-- Modal registro -->
<div class="modal fade" id="md-registro" tabindex="-1" role="dialog" aria-labelledby="md-registroLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="md-registroLabel">Regístrate</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

				<form class="form-a" action="panel/controlador/clientes.php" id="formulario_registro_cliente" method="POST">
			<div class="modal-body">
					<div clas="col-md-12 mb-0 text-center" style="display:none;text-align:center !important;" id="cont_msj">
						<span id="txt_msj" style="color:red;text-align:center;"></span>
						<br><br>
					</div>

					<div class="row">
						<div class="col-md-3 mb-0">
							<select class="form-control form-control-lg form-control-a" name="cod1" required="" id="tipo_per">
								<option selected="">V</option>
								<option>E</option>
								<option>J</option>
							</select>
						</div>

						<div class="col-md-9 mb-0">
							<div class="form-group">
								<input type="text" class="form-control form-control-lg form-control-a" placeholder="Cédula/Rif" name="cod2" required="" minlength="3">
							</div>
						</div>

						<div class="col-md-12 mb-0" style="display:none;" id="conte_razon">
							<div class="form-group">
								<label for="Correo">Razón social o empresa</label>
								<input type="text" class="form-control form-control-lg form-control-a" placeholder="Razón social o empresa" name="emp">
							</div>
						</div>

						<div class="col-md-6 mb-0">
							<div class="form-group">
								<label for="Nombres">Nombres</label>
								<input type="text" class="form-control form-control-lg form-control-a" placeholder="Nombres" required="" minlength="3" name="nom">
							</div>
						</div>

						<div class="col-md-6 mb-0">
							<div class="form-group">
								<label for="Apellidos">Apellidos</label>
								<input type="text" class="form-control form-control-lg form-control-a" placeholder="Apellidos" required="" minlength="3" name="ape">
							</div>
						</div>

						<div class="col-md-6 mb-0">
							<label for="Teléfono">Teléfono de contacto</label>
							<div class="row">
								<div class="form-group col-md-5">
									<select name="tlf1" class="form-control" required="">
										<option value="">--</option>
										<option>0412</option>
										<option>0424</option>
										<option>0414</option>
										<option>0416</option>
										<option>0426</option>
									</select>
								</div>
								<div class="form-group col-md-7">
									<input type="text" class="form-control form-control-lg form-control-a" placeholder="Teléfono" name="tlf" required="" maxlength="7" minlength="7">
								</div>
							</div>
						</div>
					
						<div class="col-md-6 mb-0">
							<div class="form-group">
								<label for="Correo">Correo</label>
								<input type="email" class="form-control form-control-lg form-control-a" placeholder="Ingresa tu correo" required="" minlength="5" name="cor">
							</div>
						</div>

						<div class="col-md-6 mb-0">
							<div class="form-group">
								<label for="Contraseña">Nueva contraseña</label>
								<input type="password" class="form-control form-control-lg form-control-a" placeholder="Ingresa tu contraseña" required="" minlength="5" name="pas">
							</div>
						</div>

						<div class="col-md-6 mb-0">
							<div class="form-group">
								<label for="Contraseña">Confirmar contraseña</label>
								<input type="password" class="form-control form-control-lg form-control-a" placeholder="Confirma tu contraseña" required="" minlength="5" name="cpa">
							</div>
						</div>

						<div class="col-sm-12">
							<p class="text-center">¿Ya tienes cuenta? <a href="">iniciar sesión</a></p>
						</div>
					</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
				<button type="submit" class="btn btn-primary" name="btg">Registrarse</button>
			</div>
				</form>
		</div>
	</div>
</div>


<!-- Modal iniciar sesión -->
<div class="modal fade" id="md-ingresar" tabindex="-1" role="dialog" aria-labelledby="md-ingresarLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="md-ingresarLabel">Iniciar sesión</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

				<form class="form-a" method="POST" action="login.php">
			<div class="modal-body">
					<div class="row">
						<div class="col-md-12 mb-0">
							<label for="Cédula">Cédula</label>
						</div>

						<div class="col-md-3 mb-2">
							<select class="form-control form-control-lg form-control-a" name="ced1" id="">
								<option selected="">V</option>
								<option>E</option>
								<option>J</option>
							</select>
						</div>

						<div class="col-md-9 mb-2">
							<div class="form-group">
								<input type="text" class="form-control form-control-lg form-control-a" placeholder="Ingresa tu cédula" required="" name="ced2">
							</div>
						</div>
						
						<div class="col-md-12 mb-2">
							<div class="form-group">
								<label for="Contraseña">Contraseña</label>
								<input type="password" class="form-control form-control-lg form-control-a" placeholder="Ingresa tu contraseña" name="pas" required="" minlength="5">
							</div>
						</div>

						<div class="col-md-12 mb-2" id="" style="color:#000;">
							<a href="#md-recuperar" data-toggle="modal" data-target="#md-recuperar" id="bt_olvido">¿Olvidó su contraseña?</a>
						</div>

						<div class="col-md-12 mb-2" id="msj_login" style="color:red;">
						</div>
					</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
				<button type="submit" class="btn btn-primary" name="btl">Iniciar sesión</button>
			</div>
				</form>
		</div>
	</div>
</div>

<!-- Modal recuperar clave -->
<div class="modal fade" id="md-recuperar" tabindex="-1" role="dialog" aria-labelledby="md-recuperarLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="md-recuperarLabel">Recuperar contraseña</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<div class="modal-body">
				<form class="form-a" method="POST" action="login.php">
					<div class="row">
						<div class="col-md-12">
							<label for="Cédula">Cédula</label>
						</div>

						<div class="col-md-3 mb-2" style="">
							<select class="form-control form-control-lg form-control-a" name="ced1" id="">
								<option selected="">V</option>
								<option>E</option>
								<option>J</option>
							</select>
						</div>

						<div class="col-md-9 mb-2">
							<div class="form-group">
								<input type="text" class="form-control form-control-lg form-control-a" placeholder="Ingresa tu cédula" required="" name="ced2" value="">
							</div>
						</div>

						<div class="modal-footer text-left">
							<button type="submit" class="btn btn-b" name="bto">Siguiente</button>
						</div>
					</div>
				</form>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
				<button type="submit" class="btn btn-primary" name="btl">Iniciar sesión</button>
			</div>
		</div>
	</div>
</div>

