<?php
  include_once("panel-admin-toyota/modelo/Repuesto.php");
  include_once("panel-admin-toyota/modelo/ModeloRepuesto.php");
  $veh = new Repuesto();
  
  $id = $_GET['id'];
  $VEH = $veh->findById($id);
  if($VEH == false){
	echo "<script>window.location ='index.php';</script>";
	exit(1);
  }

  //$mod = new Modelo();
  //$modelo = $mod->findById($VEH['id_modelo']);
  //$modelo = $modelo['modelo'];
?>

<section id="vehiculos" class="section-repuestos no-padding">
	<div class="row m-0 pb-5" style="background: #EEE;padding: 1rem;">
		<div class="col-md-12 text-center">
			<h2 class="mb-4">Repuestos</h2>
		</div>
	</div>
	<br>

	<div class="container px-md-5">
		<div class="row" style="background: #eee;padding: 2rem 0rem;">
			<div class="col-sm-12 col-md-5">
				<div class="page">
					<!--<div class="sp-loading">
						<img class="img-repuestos" src="static/images/sp-loading.gif" alt="" >
					</div>-->
					<div class="card sp-wrap">
						<a href="<?php echo $VEH['img1'];?>" target="_blank">
							<img src="<?php echo $VEH['img1'];?>" class="img-repuestos">
						</a>

						<?php if($VEH['img2'] != null) { ?>
						<a href="<?php echo $VEH['img2'];?>" target="_blank">
							<img src="<?php echo $VEH['img2'];?>" class="img-repuestos">
						</a>
						<?php }?>

						<?php if($VEH['img3'] != null) { ?>
						<a href="<?php echo $VEH['img3'];?>" target="_blank">
							<img src="<?php echo $VEH['img3'];?>" class="img-repuestos">
						</a>
						<?php }?>

						<?php if($VEH['img4'] != null) { ?>
						<a href="<?php echo $VEH['img4'];?>" target="_blank">
							<img src="<?php echo $VEH['img4'];?>" class="img-repuestos">
						</a>
						<?php }?>

						<?php if($VEH['img5'] != null) { ?>
						<a href="<?php echo $VEH['img5'];?>" target="_blank">
							<img src="<?php echo $VEH['img5'];?>" class="img-repuestos">
						</a>
						<?php }?>
					</div>
				</div>
			</div>

			<div class="col-sm-12 col-md-7">
				<h3><?php echo "<span title='codígo del repuesto'>".$VEH['cod_rep']."</span>".$VEH['nom_rep'];?></h3>
				<p><?php echo nl2br($VEH['des_rep']);?></p>
				<!--<hr>
				<center><b>Modelos y años soportados</b></center>
				  <table class="table table-stripped">
					<?php /*
						$rep = new Repuesto();
						$asignados = $rep->getModelosAsignados($_GET['id']);
						$n=0;
						while($fa = $asignados->fetch_assoc()){
						  $n++;
						  echo "<tr id='fila_".$fa['id']."'>";
							echo "<td>MODELO: </td>";
							echo "<td>".$fa['modelo']."</td>";
							echo "</tr>";
						  echo "<tr>";
							echo "<td>AÑOS: </td>";
							echo "<td>".$fa['anos_txt']."</td>";
						  echo "</tr>";
						  echo "<tr><td colspan='2'></td></tr>";
						} */
					?>
					</table>-->
				<hr>
					<h3>PRECIO: <?php echo $orm->monto($VEH['pre_rep']);?> $</h3>
				<hr>
				<a href="https://wa.link/tiuhba" target="_blank"  class="btn btn-primary">Solicitar repuesto</a>
			</div>
		</div>

		<hr>
		<div class="row">
		<div class="col-md-12">
			<h4 class="text-center">Otros repuestos</h4>
			<hr>
		</div>
		<?php
			include_once("panel-admin-toyota/modelo/Repuesto.php");	
			include_once("panel-admin-toyota/modelo/Categoria.php");	
			include_once("panel-admin-toyota/modelo/ModeloRepuesto.php");	
			$repuesto = new Repuesto();
			$categoria = new Categoria();
			$modeloR = new ModeloRepuesto();
			$resultados = $repuesto->getUltimosPublicados(6);
				while($data = $resultados->fetch_assoc()){
				?>
				<div class="col-md-4 mb-3">
					<a href="?op=repuesto-ver&id=<?php echo $data['id'];?>">
						<div class="card">
							<div class="text-center">
								<img class="card-img-top img-repuestos" src="<?php echo $data['img1'];?>" alt="">
							</div>

							<div class="card-body p-3">
								<h5 class="card-title"><?php echo $data['nom_rep'];?></h5>
								<!--<b class="" style="color:#000;">CÓDIGO: <?php // echo $data['cod_rep'];?></b>-->
								<p class="card-text"><?php echo substr($data['des_rep'], 0, 30);?>. . .</p>
								<h3 class="card-text"><b>U$S <?php echo substr($data['pre_rep'], 0, 30);?></b></h3>
							</div>

							<div class="card-footer p-3">
								<a href="https://wa.link/tiuhba" target="_blank" class="btn btn-primary">Me interesa</a>
							</div>
						</div>
					</a>
					<br>
				</div>
				<?php } ?>
			</div>

		<div class="col-sm-12 text-center mt-5">
			<hr><a href="?op=repuestos" class="btn btn-primary">Volver a repuestos</a><br><br>
		</div>
	</div>
</section>
