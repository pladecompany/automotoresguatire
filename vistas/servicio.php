<div id="banner-area" class="banner-area" style="background-image:url(static/img/banner/banner-servicio.jpg)">
	<div class="banner-text">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="banner-heading">
						<h1 class="banner-title">Servicio</h1>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<section id="main-container" class="main-container">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<h3 class="column-title">Servicio TOYOTA</h3>
				<p>Automotores Guatire tenemos las mejores instalaciones para atender a tu Toyota que sumado al personal técnico  capacitado en Toyota de Venezuela, las herramientas especiales y la tecnología hacen que tengamos un servicio de calidad esperado. </p>

				<p>Contamos con servicio especializado Toyota desde un simple cambio de aceite y filtro hasta la reconstrucción de motores. </p>

				<h5>Otros servicios:</h5>
				<p>Limpieza de inyectores, rectificación de discos, alineacion y balanceo, montaje y desmontaje de cauchos, Aire acondicionado, Sanitización de vehículos.</p>
				<p>Para mejor atención contamos con un teléfono WhatsApp para que agendes tu cita y hagas seguimiento a tu servicio.</p>
				<h4 class="text-center">Escríbenos - 0412-3857282</h4>
			</div>

			<div class="col-md-6">
				<div id="page-slider" class="owl-carousel owl-theme page-slider small-bg">
					<div class="item" style="background-image:url(static/img/slider-pages/slide-servicio-1.jpg)">
						<div class="container">
							<div class="box-slider-content">
								<div class="box-slider-text">
									<h2 class="box-slide-title">Servicio TOYOTA</h2>
								</div>    
							</div>
						</div>
					</div>

				<div class="item" style="background-image:url(static/img/slider-pages/slide-servicio-2.jpg)">
					<div class="container">
						<div class="box-slider-content">
							<div class="box-slider-text">
								<h2 class="box-slide-title">Servicio TOYOTA</h2>
							</div>
						</div>
					</div>
				</div>

				<div class="item" style="background-image:url(static/img/slider-pages/slide.jpg)">
					<div class="container">
						<div class="box-slider-content">
							<div class="box-slider-text">
								<h2 class="box-slide-title">Automotores Guatire</h2>
							</div>    
						</div>
					</div>
				</div>

				<div class="item" style="background-image:url(static/img/slider-pages/slide-2.jpg)">
					<div class="container">
						<div class="box-slider-content">
							<div class="box-slider-text">
								<h2 class="box-slide-title">Automotores Guatire</h2>
							</div>    
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>