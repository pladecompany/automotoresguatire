<section id="contacto" class="main-container">
	<div class="container">
		<div class="row text-center">
			<h2 class="section-title">Ubícanos y comunícanos</h2>
			<h3 class="section-sub-title">Contacto</h3>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="ts-service-box-bg text-center">
					<span class="ts-service-icon icon-round">
						<i class="fa fa-map-marker-alt"></i>
					</span>
					<div class="ts-service-box-content">
						<h4>Visita nuestra oficina</h4>
						<p>Av. intercomunal Guarenas Guatire, sector vega abajo, al lado del Centro comercial Oasis, Guatire, Edo. Miranda</p>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="ts-service-box-bg text-center">
					<span class="ts-service-icon icon-round">
						<i class="fab fa-whatsapp"></i>
					</span>
					<div class="ts-service-box-content">
						<h4>Escríbenos</h4>
						<p><small>Departamento de servicio</small><br><b><i class="fab fa-whatsapp"></i> 0412-3857282</b></p>
						<p><small>Departamento de repuestos</small><br><b><i class="fab fa-whatsapp"></i> 0412-9009573</b></p>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="ts-service-box-bg text-center">
					<span class="ts-service-icon icon-round">
						<i class="fa fa-phone"></i>
					</span>
					<div class="ts-service-box-content">
						<h4>Llámanos</h4>
						<p>0212-9351517 / 0212-9351518</p>
						<p>0212-9351522 / 0212-9361509</p>
					</div>
				</div>
			</div>
		</div>

		<div class="gap-60"></div>


		<div class="gap-40"></div>

		<div class="row" id="contacto">
			<div class="col-md-12">
			<h3 class="column-title">Contáctanos</h3>

			<form id="contact-form" action="panel/controlador/contacto.php" method="post" role="form">
				<div class="error-container"></div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
						<label>Nombre</label>
						<input class="form-control form-control-name" name="name" id="name" placeholder="" type="text" required>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<label for="ced">Departamento que desea comunicar</label>
							<select class="form-control form-control-departamento form-control-lg form-control-a" id="departamento" name="departamento" required>
								<option value="" selected>--Selecciona el departamento--</option>
								<option>Servicio</option>
								<option>Repuestos</option>
								<option>Vehículos</option>
							</select>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
						<label>Correo electrónico</label>
						<input class="form-control form-control-email" name="email" id="email" 
						placeholder="" type="email" required>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
						<label>Asunto</label>
						<input class="form-control form-control-subject" name="subject" id="subject" 
						placeholder="" required>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label>Mensaje</label>
					<textarea class="form-control form-control-message" name="message" id="message" placeholder="" rows="10" required></textarea>
				</div>

				<div class="text-right"><br>
					<button class="btn btn-primary solid blank" type="submit">Enviar mensaje</button> 
				</div>
			</form>
			</div>
		</div>
	</div>
</section>