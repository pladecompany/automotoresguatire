<?php
	error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING ^ E_DEPRECATED);
	
	$opcion = $_GET['op'];

    if(($opcion =="ver_reclamo" ||$opcion == "cita" || $opcion=="perfil" || $opcion=="misvehiculos" || $opcion == "inicio_log" ||$opcion == "reclamos") && !isset($_SESSION['log'])){
      echo "<script>window.location ='index.php';</script>";
      exit(1);
    }

	switch ($opcion) {
		case "inicio_log":
			$ruta="vistas/inicio_log.php";
		break;

		case "inicio":
			$ruta="vistas/inicio.php";
		break;

		case "servicio":
			$ruta="vistas/servicio.php";
		break;

		case "repuestos":
			$ruta="vistas/repuestos.php";
		break;

		case "repuesto-ver":
			$ruta="vistas/repuesto-ver.php";
		break;

		case "vehiculos":
			$ruta="vistas/vehiculos.php";
		break;

		case "reclamos":
			$ruta="vistas/reclamos.php";
		break;

		case "misvehiculos":
			$ruta="vistas/misvehiculos.php";
		break;

		case "perfil":
			$ruta="vistas/perfil.php";
		break;

		case "citas":
			$ruta="vistas/citas.php";
		break;

		default:
			$ruta="vistas/inicio.php";
		break;
	}
?>
