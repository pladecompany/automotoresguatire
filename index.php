<?php
	error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING ^ E_DEPRECATED);
	session_start();

	/*if (empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] === "off") {
		$location = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		header('HTTP/1.1 301 Moved Permanently');
		header('Location: ' . $location);
		exit;
	}*/


	include_once('ruta.php');



?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>Automotores Guatire</title>
	<link rel="icon" href="static/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

	<link rel="stylesheet" href="static/css/bootstrap.min.css">
	<link rel="stylesheet" href="static/css/style.css">
	<link rel="stylesheet" href="static/css/responsive.css">
	<link rel="stylesheet" href="static/css/font-awesome.min.css">
	<link rel="stylesheet" href="static/css/animate.css">
	<link rel="stylesheet" href="static/css/owl.carousel.min.css">
	<link rel="stylesheet" href="static/css/owl.theme.default.min.css">
	<link rel="stylesheet" href="static/css/colorbox.css">
	<script type="text/javascript" src="static/js/jquery.js"></script>
    <script src='fullcalendar/packages/moment.js'></script>

</head>

<body>
	<div class="body-inner">
		<div id="top-bar" class="top-bar hidden-mv">
			<div class="container">
				<div class="row">
					<div class="col-lg-10 col-md-10 col-sm-6 col-xs-12">
						<ul class="top-info">
							<li>
								<p class="info-text"><i class="fa fa-map-marker-alt">&nbsp;</i> AV. Intercomunal Guarenas Guatire, local Automotores Guatire, Urb. Vega, Abajo, al lado del C.C La Parada.Edo.Miranda., Guatire 1221, Miranda</p>
							</li>
						</ul>
					</div>

					<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 top-social text-right">
						<ul class="unstyled">
							<li>
								<a title="Facebook" href="https://facebook.com/toyoguatire" target="_blank">
									<span class="social-icon"><i class="fab fa-facebook"></i></span>
								</a>
								<a title="Instagram" href="https://instagram.com/toyoguatire" target="_blank">
									<span class="social-icon"><i class="fab fa-instagram"></i></span>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<header id="header" class="header-one">
			<div class="container hidden-mv">
				<div class="row">
					<div class="logo-area clearfix">
						<div class="col-xs-12 col-md-12 header-right">
							<ul class="top-info-box">
								<li>
									<div class="info-box">
										<div class="info-box-content">
											<a href="?op=inicio">
												<img src="static/img/logo-nav.png" alt="" width="100%">
											</a>
										</div>
									</div>
								</li>

								<li class="hidden-mv">
									<div class="info-box">
										<div class="info-box-content">
											<p class="info-box-title">Llámanos</p>
											<p class="info-box-subtitle">0212-9351517</p>
										</div>
									</div>
								</li>

								<li class="hidden-mv">
									<div class="info-box">
										<div class="info-box-content">
											<p class="info-box-title">Escríbenos</p>
											<p class="info-box-subtitle">info@automotoresguatire.com.ve</p>
										</div>
									</div>
								</li>

								<li class="last hidden-mv">
									<div class="info-box last">
										<div class="info-box-content">
											<p class="info-box-title">Horario</p>
											<p class="info-box-subtitle">Lunes a viernes / 7:00–12:00, 13:30–17:00</p>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<nav class="site-navigation navigation navdown">
				<div class="container">
					<div class="row">
						<div class="col-sm-12">
							<div class="site-nav-inner text-center">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>

								<a class="navbar-brand hidden-lp" href="?op=inicio">
									<img src="static/img/logo-nav.png" alt="" width="80%">
								</a>

								<div class="collapse navbar-collapse navbar-responsive-collapse">
									<ul class="nav navbar-nav text-center">
										<li><a href="?op=inicio">Inicio</a></li>
										<li><a href="?op=servicio">Servicio</a></li>
										<li><a href="?op=vehiculos">Vehículos</a></li>
										<li><a href="?op=repuestos">Repuestos</a></li>
										<li><a href="?op=inicio#contacto">Contacto</a></li>
										<?php
											if(isset($_SESSION['log']) && $_SESSION['log'] == true){
											}else{
										?>
											<li><a href="#" data-toggle="modal" data-target="#md-ingresar">Iniciar sesión</a></li>
											<li><a href="#" data-toggle="modal" data-target="#md-registro">Regístrate</a></li>

										<?php }?>
									</ul>
									<?php
										if(isset($_SESSION['log'])&&$_SESSION['log'] == true){
									?>
										<button onclick="window.location='?op=inicio_log';" type="button" class="btn btn-primary" style="color:#fff;"> <?php echo "Hola ". $_SESSION['nom'];?> </button>
									<?php
									}else{
									?>
									<?php }?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</nav>
		</header>

		<?php include($ruta); ?>

		<footer id="footer" class="footer bg-overlay">
			<div class="footer-main">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-sm-12 footer-widget footer-about">
							<h3 class="widget-title">Nosotros</h3>
							<img class="footer-logo" src="static/img/logo-nav.png" alt="" />
							<div class="footer-social">
								<ul>
									<li><a href="https://facebook.com/toyoguatire" target="_blank"><i class="fab fa-facebook"></i></a></li>
									<li><a href="https://instagram.com/toyoguatire" target="_blank"><i class="fab fa-instagram"></i></a></li>
								</ul>
							</div>
						</div>

						<div class="col-md-4 col-sm-12 footer-widget">
							<h3 class="widget-title">Horario de trabajo</h3>
							<div class="working-hours">
								<p class="m-0">Trabajamos 5 días hábiles de la semana, el horario puede cambiar durante el feriado, puedes contactarnos a nuestros teléfonos o correo si tienes una emergencia o necesitas una cita.</p>
								<p class="m-0"><b>Lunes a viernes</b></p>
								<p class="m-0">7:00-12:00 / 13:30-17:00</p>
							</div>
						</div>

						<div class="col-md-4 col-sm-12 footer-widget">
							<h3 class="widget-title">Ofrecemos</h3>
							<ul class="list-arrow">
								<li><a href="?op=servicio"><i class="fa fa-angle-down"></i> Servicio</a></li>
								<li><a href="?op=repuestos"><i class="fa fa-angle-down"></i> Repuestos</a></li>
								<li><a href="?op=vehiculos"><i class="fa fa-angle-down"></i> Vehículos</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="copyright">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12">
							<div class="copyright-info text-center">
								<span>Copyright © 2020 desarrollado por <a href="https://pladecompany.com" class='text-white' style="color:#ffF;" target="_blank">pladecompany.com</a></span>
							</div>
						</div>
					</div>

					<div id="back-to-top" data-spy="affix" data-offset-top="10" class="back-to-top affix">
						<button class="btn btn-primary" title="Back to Top">
							<i class="fa fa-angle-double-up"></i>
						</button>
					</div>
				</div>
			</div>
		</footer>

		<script type="text/javascript" src="static/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="static/js/owl.carousel.min.js"></script>
		<script type="text/javascript" src="static/js/jquery.colorbox.js"></script>
		<script type="text/javascript" src="static/js/custom.js"></script>
	</div>
</body>
</html>
