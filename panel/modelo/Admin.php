<?php
  include_once("Orm.php");
  include_once("Conexion.php");

  class Admin{
    private $tabla = "admins";
    public $data = [];
    public $niveles = array('0'=>'Administrador', '1'=>'Gestión de citas', '2'=>'Gestión de vehículos');
    public $orm = null;

    public function Admin(){
      $this->orm = new Orm(new Conexion());
      $tihs->data = [];
    }

    public function login($u, $p){
      $sql = "SELECT * FROM admins WHERE cor_adm='$u' AND pas_adm=md5('$p');";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return $r->fetch_assoc();
      }else{
        return false;
      }
    }
    public function removeById($id){
      return $this->orm->eliminar('id', $id, $this->tabla);
    }

    public function getNiveles(){
      return $this->niveles;
    }

    public function getNivel($index){
      return $this->niveles[$index];
    }

    public function actualizarPassword($n, $a, $ida){
      $sql = "UPDATE admins SET pas_adm=md5('$n') WHERE id='$ida' AND pas_adm=md5('$a');";
      return $this->orm->editarPersonalizado($sql);
    }

    public function fetchAll(){
      $sql = "SELECT * FROM ".$this->tabla." ORDER BY id DESC;";
      return $this->orm->consultaPersonalizada($sql);
    }
      public function edit($id){
        $sql = "UPDATE ".$this->tabla." SET ";
        $i = 0;
        $n = count($this->data);
        foreach($this->data as $key => $index){
          $i++;
          $sql.= $key."='$index'";
          if($i < $n){
            $sql.= ",";
          }
        }
      $sql.= " WHERE id='$id';";
      return $this->orm->editarPersonalizado($sql);

    }
    public function findById($id){
      $sql = "SELECT * FROM " . $this->tabla . " WHERE id='$id';";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return $r->fetch_assoc();
      }else{
        return false;
      }
    }
    public function save(){
      $sql = "INSERT INTO " . $this->tabla ." VALUES(";
      $i = 0;
      $n = count($this->data);
      foreach($this->data as $key => $index){
        $i++;
        if($index == "")
          $sql.= "null";
        else
          $sql.= "'$index'";
        if($i < $n){
          $sql.= ",";
        }
      }
      $sql.= ");";
      return $this->orm->insertarPersonalizado($sql);
    }


  }
?>
