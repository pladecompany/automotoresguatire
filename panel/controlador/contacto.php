<?php

if(isset($_POST)){
    include_once("../modelo/Orm.php");
    $nom = $_POST['name'];
    $dep = $_POST['departamento'];
    $email = $_POST['email'];
    $asunto = $_POST['subject'];
    $mess = $_POST['message'];

    $mensaje = '';
    $mensaje.= '
        <ul>
            <li>Nombre y apellido del cliente: '.$nom.'</li>
            <li>Departamento que desea comunicar: '.$dep.'</li>
            <li>Correo: '.$email.'</li>
            <li>Mensaje que envia: '.$mess.'</li>
        </ul>
    ';

    $para = "info@automotoresguatire.com.ve";
    
    $resultado = $orm->enviarCorreo($para, $mensaje, $asunto);

    if(!$resultado){
        echo "<script>alert('No se ha podido enviar su correo, intentelo mas tarde.')</script>";
    } else {
        echo "<script>alert('Su correo fue envia cocrrectamente a info@automotoresguatire.com.ve, le contactaremos a la brevedad posible.')</script>";
        echo "<script>window.location = 'index.php';</script>";
    }
}