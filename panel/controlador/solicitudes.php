<?php
  include_once("panel/modelo/Solicitudes.php"); 
  include_once("panel/modelo/MiVehiculo.php"); 
  include_once("panel/modelo/Conexion.php"); 
  include_once("panel/modelo/Cliente.php");

  if(isset($_POST) && isset($_POST['btg'])){

    $v = $_POST['cod1'];
    $f = $_POST['fec'];
    $d = $_POST['dep'];
    $de = $_POST['descrip'];

    $cliente = new Solicitudes();
    $cliente->data["id"] = "";
    $cliente->data["id_vehiculo"] = $v;
    $cliente->data["id_usuario"] = $_SESSION['idu'];
    $cliente->data["fec_sol"] = $f;
    $cliente->data["depar"] = $d;
    $cliente->data["descrip"] = $de;
    
    $r = $cliente->save();
    if($r->affected_rows == 1){
      $id = $r->insert_id;
      $veh = new MiVehiculo();
      $cli = new Cliente();
      $r = $veh->findByIdAll($v);
      $F = $cli->findById($_SESSION['idu']);
      $mensaje='¡Nueva solicitud de cita!<br><br>
      <b>ID usuario</b>: '.$_SESSION['idu'].'<br>
      <b>Nombres</b>: '.$F['nom_usu'].'<br>
      <b>Apellidos</b>: '.$F['ape_usu'].'<br>
      <b>Teléfono</b>: '.$F['tel_usu'].'<br>
      <b>Correo</b>: '.$F['cor_usu'].'<br>
      <b>Vehiculo</b>: '.$r['modelo'].' / '.$r['placa'].'<br>
      <b>Fecha solicitud</b>: '.$f.'<br>
      <b>Departamento</b>: '.$d.'<br>
      <b>Descripción</b>: '.$de.'<br>';
      $mm = $orm->enviarCorreo('info@automotoresguatire.com.ve', $mensaje, 'Nueva solicitud de cita');
      $err = "¡Su solicitud de cita fue enviada correctamente, nos comunicaremos con usted a la brevedad posible!g";
      echo "<script>window.location ='?op=citas&info&msj=$err';</script>";
      exit(1);
    }else{
      $err = "Error al registrar";
      echo "<script>window.location ='?op=citas&err&msj=$err';</script>";
      exit(1);
    }

  }

?>
