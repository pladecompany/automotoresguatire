<?php
  include_once("modelo/Repuesto.php"); 
  include_once("modelo/Categoria.php"); 
  include_once("modelo/Conexion.php"); 

  if(isset($_POST) && isset($_POST['btg'])){

    $nom = $_POST['nom'];
    $idm = $_POST['mod'];
    $ano = $_POST['ano'];
    $pre = $_POST['pre'];
    $est = $_POST['est'];
    $des = $_POST['des'];



    $cliente = new Repuesto();
    $cliente->data["id"] = "";
    $cliente->data["id_categoria"] = $idm;
    $cliente->data["nom_rep"] = $nom;
    $cliente->data["des_rep"] = $des;
    $cliente->data["ano_rep"] = "";
    $cliente->data["pre_rep"] = $pre;
    $cliente->data["cam_1"] = '';
    $cliente->data["cam_2"] = '';
    $cliente->data["cam_3"] = '';
    $cliente->data["img1"] = null;
    $cliente->data["img2"] = null;
    $cliente->data["img3"] = null;
    $cliente->data["img4"] = null;
    $cliente->data["img5"] = null;
    $cliente->data["est_rep"] = $est;

    $r = $cliente->save();
    $idr = $r->insert_id;
    if($r->affected_rows == 1){
      $id = $r->insert_id;
      if(!empty($_FILES['img1'])){
        $orm = new Orm(new Conexion());
        $ruta = getcwd() . "/../static/img/files/";
        $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['img1']['name']); 
        $nombre = $ruta . $nom1;
        $nf = $orm->obtenerDominio()."/static/img/files/".$nom1;
        if(move_uploaded_file($_FILES['img1']['tmp_name'], $nombre)) {
          $producto = new Repuesto();
          $producto->data['img1'] = $nf;
          $producto->edit($id);
        }
      }
      if(!empty($_FILES['img2'])){
        $orm = new Orm(new Conexion());
        $ruta = getcwd() . "/../static/img/files/";
        $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['img2']['name']); 
        $nombre = $ruta . $nom1;
        $nf = $orm->obtenerDominio()."/static/img/files/".$nom1;
        if(move_uploaded_file($_FILES['img2']['tmp_name'], $nombre)) {
          $producto = new Repuesto();
          $producto->data['img2'] = $nf;
          $producto->edit($id);
        }
      }
      if(!empty($_FILES['img3'])){
        $orm = new Orm(new Conexion());
        $ruta = getcwd() . "/../static/img/files/";
        $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['img3']['name']); 
        $nombre = $ruta . $nom1;
        $nf = $orm->obtenerDominio()."/static/img/files/".$nom1;
        if(move_uploaded_file($_FILES['img3']['tmp_name'], $nombre)) {
          $producto = new Repuesto();
          $producto->data['img3'] = $nf;
          $producto->edit($id);
        }
      }
      if(!empty($_FILES['img4'])){
        $orm = new Orm(new Conexion());
        $ruta = getcwd() . "/../static/img/files/";
        $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['img4']['name']); 
        $nombre = $ruta . $nom1;
        $nf = $orm->obtenerDominio()."/static/img/files/".$nom1;
        if(move_uploaded_file($_FILES['img4']['tmp_name'], $nombre)) {
          $producto = new Repuesto();
          $producto->data['img4'] = $nf;
          $producto->edit($id);
        }
      }
      if(!empty($_FILES['img5'])){
        $orm = new Orm(new Conexion());
        $ruta = getcwd() . "/../static/img/files/";
        $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['img5']['name']); 
        $nombre = $ruta . $nom1;
        $nf = $orm->obtenerDominio()."/static/img/files/".$nom1;
        if(move_uploaded_file($_FILES['img5']['tmp_name'], $nombre)) {
          $producto = new Repuesto();
          $producto->data['img5'] = $nf;
          $producto->edit($id);
        }
      }

      if(!empty($_FILES['doc'])){
        $orm = new Orm(new Conexion());
        $ruta = getcwd() . "/../static/img/files/";
        $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['doc']['name']); 
        $nombre = $ruta . $nom1;
        $nf = $orm->obtenerDominio()."/static/img/files/".$nom1;
        if(move_uploaded_file($_FILES['doc']['tmp_name'], $nombre)) {
          $producto = new Repuesto();
          $producto->data['doc'] = $nf;
          $producto->edit($id);
        }
      }
      $producto = new Repuesto();
      $modelos = $_POST['seleccionados'];
      $anos = $_POST['anos'];
      $producto->asignarModelos($idr, $modelos, $anos);
      $err = "¡Registró correctamente!";
      echo "<script>window.location ='?op=nuevo_repuesto&info&id=$idr&msj=$err';</script>";
      exit(1);
    }else{
      $err = "¡Código/Correo ya existe!";
      echo "<script>window.location ='?op=nuevo_repuesto&err&msj=$err';</script>";
      exit(1);
    }

  }else if(isset($_POST) && isset($_POST['btc'])){
    $nom = $_POST['nom'];
    $idm = $_POST['mod'];
    $ano = $_POST['ano'];
    $pre = $_POST['pre'];
    $est = $_POST['est'];
    $des = $_POST['des'];


    $cliente = new Repuesto();
    $id = $_POST['idn'];
    $cliente->data["id_categoria"] = $idm;
    $cliente->data["nom_rep"] = $nom;
    $cliente->data["des_rep"] = $des;
    $cliente->data["pre_rep"] = $pre;
    $cliente->data["est_rep"] = $est;
    $r = $cliente->edit($id);

      $producto = new Repuesto();
      $modelos = $_POST['seleccionados'];
      $anos = $_POST['anos'];
      $producto->asignarModelos($id, $modelos, $anos);

      if(!empty($_FILES['img1'])){
        $orm = new Orm(new Conexion());
        $ruta = getcwd() . "/../static/img/files/";
        $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['img1']['name']); 
        $nombre = $ruta . $nom1;
        $nf = $orm->obtenerDominio()."/static/img/files/".$nom1;
        if(move_uploaded_file($_FILES['img1']['tmp_name'], $nombre)) {
          $producto = new Repuesto();
          $producto->data['img1'] = $nf;
          $producto->edit($id);
        }
      }
      if(!empty($_FILES['img2'])){
        $orm = new Orm(new Conexion());
        $ruta = getcwd() . "/../static/img/files/";
        $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['img2']['name']); 
        $nombre = $ruta . $nom1;
        $nf = $orm->obtenerDominio()."/static/img/files/".$nom1;
        if(move_uploaded_file($_FILES['img2']['tmp_name'], $nombre)) {
          $producto = new Repuesto();
          $producto->data['img2'] = $nf;
          $producto->edit($id);
        }
      }
      if(!empty($_FILES['img3'])){
        $orm = new Orm(new Conexion());
        $ruta = getcwd() . "/../static/img/files/";
        $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['img3']['name']); 
        $nombre = $ruta . $nom1;
        $nf = $orm->obtenerDominio()."/static/img/files/".$nom1;
        if(move_uploaded_file($_FILES['img3']['tmp_name'], $nombre)) {
          $producto = new Repuesto();
          $producto->data['img3'] = $nf;
          $producto->edit($id);
        }
      }
      if(!empty($_FILES['img4'])){
        $orm = new Orm(new Conexion());
        $ruta = getcwd() . "/../static/img/files/";
        $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['img4']['name']); 
        $nombre = $ruta . $nom1;
        $nf = $orm->obtenerDominio()."/static/img/files/".$nom1;
        if(move_uploaded_file($_FILES['img4']['tmp_name'], $nombre)) {
          $producto = new Repuesto();
          $producto->data['img4'] = $nf;
          $producto->edit($id);
        }
      }
      if(!empty($_FILES['img5'])){
        $orm = new Orm(new Conexion());
        $ruta = getcwd() . "/../static/img/files/";
        $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['img5']['name']); 
        $nombre = $ruta . $nom1;
        $nf = $orm->obtenerDominio()."/static/img/files/".$nom1;
        if(move_uploaded_file($_FILES['img5']['tmp_name'], $nombre)) {
          $producto = new Repuesto();
          $producto->data['img5'] = $nf;
          $producto->edit($id);
        }
      }
      if(!empty($_FILES['doc'])){
        $orm = new Orm(new Conexion());
        $ruta = getcwd() . "/../static/img/files/";
        $nom1 = date('Y_m_d_H_i_s') . basename($_FILES['doc']['name']); 
        $nombre = $ruta . $nom1;
        $nf = $orm->obtenerDominio()."/static/img/files/".$nom1;
        if(move_uploaded_file($_FILES['doc']['tmp_name'], $nombre)) {
          $producto = new Repuesto();
          $producto->data['doc'] = $nf;
          $producto->edit($id);
        }
      }

    if($r==true){
      $err = "¡Información actualizada!";
      echo "<script>window.location ='?op=nuevo_repuesto&id=$id&info&msj=$err';</script>";
    }else{
      $err = "No se realizó ningún cambio.";
      echo "<script>window.location ='?op=nuevo_repuesto&id=$id&info&msj=$err';</script>";
    }
    exit(1);


  }else if(isset($_GET['id'])){
    $id = $_GET['id'];
    $cliente = new Repuesto();
    $r = $cliente->findById($id);
    if($r==false){
      $err = "No existe ningún registro con el ID ($id).";
      echo "<script>window.location ='?op=nuevo_repuesto&err&msj=$err';</script>";
      exit(1);
    }else{
      $F = $r;
    }
  }else if(isset($_GET['el'])){
    $id = $_GET['el'];
    $cliente = new Repuesto();
    if($cliente->removeById($id)){
      $err = "¡Eliminado correctamente!";
      echo "<script>window.location ='?op=repuestos&info&msj=$err';</script>";
    }else{
      $err = "El registro no puede ser eliminado, tiene información en uso.";
      echo "<script>window.location ='?op=repuestos&err&msj=$err';</script>";
    }
    exit(1);
  }

?>
