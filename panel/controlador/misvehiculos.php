<?php
  include_once("panel/modelo/MiVehiculo.php"); 
  include_once("panel/modelo/Modelo.php"); 
  include_once("panel/modelo/ModeloCitas.php"); 
  include_once("panel/modelo/Conexion.php"); 

  if(isset($_GET['cambio'])){
    $placa = $_GET['cambio'];
    
    $cliente = new MiVehiculo();
    $veh = $cliente->findByPlaca($placa);

    if($veh!= false){
      $id= $veh['id'];
      $cliente->data["id_usuario"] = $_SESSION['idu'];
      $r = $cliente->edit($id);
      if($r==true){
        $err = "¡Información actualizada, ahora el vehículo se asigno a su nombre!";
        echo "<script>window.location ='?op=misvehiculos&id=$id&info&msj=$err';</script>";
      }else{
        $err = "No se realizó ningún cambio.";
        echo "<script>window.location ='?op=misvehiculos&id=$id&info&msj=$err';</script>";
      }
    }else{
      $err = "Ocurrio un error, con el cambio del vehículo";
      echo "<script>window.location ='?op=misvehiculos&id=$id&info&msj=$err';</script>";
    }
    exit(1);
  }

  if(isset($_POST) && isset($_POST['btg'])){

    $pla = strtoupper($_POST['pla']);
    $mod = $_POST['mod'];
    $se1 = $_POST['ser1'];
    $se2 = $_POST['ser2'];
    $se3 = $_POST['ser3'];
    $se4 = null;
    $ano = $_POST['ano'];

    if(strlen($pla) == 0){
      $err = "Placa: Debe llenar el campo modelo.";
    }else if(strlen($se1) == 0){
      $err = "Serial 1: Debe llenar el campo modelo.";
    }


    if(isset($err)){
      echo "<script>window.location ='?op=misvehiculos&err&msj=$err';</script>";
      exit(1);
    }

    $cliente = new MiVehiculo();
    $cliente->data["id"] = "";
    $cliente->data["id_usuario"] = $_SESSION['idu'];
    $cliente->data["id_modelo"] = $mod;
    $cliente->data["placa"] = $pla;
    $cliente->data["serial1"] = $se1;
    $cliente->data["serial2"] = $se2;
    $cliente->data["serial3"] = $se3;
    $cliente->data["serial4"] = $se4;
    $cliente->data["ano"] = $ano;

    $r = $cliente->save();
    if($r->affected_rows == 1){
      $id = $r->insert_id;
      $err = "¡Registró correctamente!";
      echo "<script>window.location ='?op=misvehiculos&info&msj=$err';</script>";
      exit(1);
    }else{
      $err = "La placa ya existe";
      echo "<script>window.location ='?op=misvehiculos&err&msj=$err';</script>";
      exit(1);
    }

  }else if(isset($_POST) && isset($_POST['btc'])){

    $idn = $_POST['idn'];
    $pla = strtoupper($_POST['pla']);
    $mod = $_POST['mod'];
    $se1 = $_POST['ser1'];
    $se2 = $_POST['ser2'];
    $se3 = $_POST['ser3'];
    $se4 = null;
    $ano = $_POST['ano'];

    if(strlen($pla) == 0){
      $err = "Placa: Debe llenar el campo modelo.";
    }else if(strlen($se1) == 0){
      $err = "Serial 1: Debe llenar el campo modelo.";
    }

    $id = $idn;
    if(isset($err)){
      echo "<script>window.location ='?op=misvehiculos&err&msj=$err';</script>";
      exit(1);
    }

    $cliente = new MiVehiculo();
    $cliente->data["id_modelo"] = $mod;
    $cliente->data["placa"] = $pla;
    $cliente->data["serial1"] = $se1;
    $cliente->data["serial2"] = $se2;
    $cliente->data["serial3"] = $se3;
    $cliente->data["serial4"] = $se4;
    $cliente->data["ano"] = $ano;

    $r = $cliente->edit($id);
    if($r==true){
      $err = "¡Información actualizada!";
      echo "<script>window.location ='?op=misvehiculos&id=$id&info&msj=$err';</script>";
    }else{
      $err = "No se realizó ningún cambio.";
      echo "<script>window.location ='?op=misvehiculos&id=$id&info&msj=$err';</script>";
    }
    exit(1);
  }else if(isset($_GET['id'])){
    $id = $_GET['id'];
    $cliente = new MiVehiculo();
    $r = $cliente->findById($id);
    if($r==false){
      $err = "No existe ningún registro con el ID ($id).";
      echo "<script>window.location ='?op=misvehiculos&err&msj=$err';</script>";
      exit(1);
    }else{
      $F = $r;
    }
  }else if(isset($_GET['el'])){
    $id = $_GET['el'];
    $cliente = new MiVehiculo();
    if($cliente->removeById($id)){
      $err = "¡Eliminado correctamente!";
      echo "<script>window.location ='?op=misvehiculos&info&msj=$err';</script>";
    }else{
      $err = "El registro no puede ser eliminado, tiene información en uso.";
      echo "<script>window.location ='?op=misvehiculos&err&msj=$err';</script>";
    }
    exit(1);
  }

?>
