<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h4 class="m-0 font-weight-bold color-b">Cambiar contraseña</h4>
	</div>

	<div class="card-body">
		<div class="row">
			<div class="col-sm-12 col-md-6 offset-md-3">
				<form class="form-a">
					<div class="row">
						<div class="col-md-12 mb-2">
							<div class="form-group">
								<label for="Contraseña">Ingresa tu contraseña actual</label>
								<input type="password" class="form-control form-control-lg form-control-a" placeholder="Ingresa tu contraseña">
							</div>
						</div>

						<div class="col-md-12 mb-2">
							<div class="form-group">
								<label for="Contraseña">Ingresa tu nueva contraseña</label>
								<input type="password" class="form-control form-control-lg form-control-a" placeholder="Ingresa tu contraseña">
							</div>
						</div>

						<div class="col-md-12 mb-2">
							<div class="form-group">
								<label for="Contraseña">Repite tu nueva contraseña</label>
								<input type="password" class="form-control form-control-lg form-control-a" placeholder="Ingresa tu contraseña">
							</div>
						</div>

						<div class="col-md-12">
							<button type="submit" class="btn btn-primary">Guardar</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>