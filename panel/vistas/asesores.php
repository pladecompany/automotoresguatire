<?php
  include_once("controlador/asesores.php");
?>
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h4 class="m-0 font-weight-bold color-b">Asesores</h4>
		
		<div class="text-right">
			<a href="#md-noticia" data-toggle="modal" class="color-b modal-trigger" id="bt_nueva_noticia"><b><i class="fa fa-plus-circle"></i> Registrar asesor</b></a>
		</div>
	</div>

	<div class="card-body">
        <?php include_once("vistas/mensajes.php");?>
		<div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th>#</th>
						<th>Codígo</th>
						<th>Nombre</th>
						<th>Apellido</th>
						<th>Teléfono</th>
						<th>Estatus</th>
						<th>Imagén</th>
						<th>Acciones</th>
					</tr>
				</thead>

				<tbody>
                  <?php
                    $noti = new Asesor();
                    $r = $noti->fetchAll();
                    $i=0;
                    while($ff = $r->fetch_assoc()){
                      $i++;
                      echo "<tr>";
                      echo "  <td>" . $i . "</td>";
                      echo "  <td>" . $ff['cod_age'] . "</td>";
                      echo "  <td>" . $ff['nom_age'] . "</td>";
                      echo "  <td>" . $ff['ape_age'] . "</td>";
                      echo "  <td>" . $ff['tlf_age'] . "</td>";
                      echo "  <td>" . (($ff['est_age']==1)? 'Activo':'Inactivo') . "</td>";
                      echo "  <td><img src='" . $ff['img_age'] . "' style='width:50px;'></td>";
                      echo "  <td>";
                      echo "<a href='?op=asesores&id=".$ff['id']."'><i class='mr-2 fa fa-edit'></i></a>";
                      echo "<a href='?op=asesores&el=".$ff['id']."' onclick='return confirm(\"¿ Esta seguro ?\")'><i class='mr-2 fa fa-trash'></i></a>";
                      echo "</td>";
                      echo "</tr>";
                    }
                  ?>
				</tbody>
			</table>
		</div>
	</div>
</div>


<div id="md-noticia" class="modal modalmedium fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="title-box-d">
                    <?php if(isset($F)){ ?>
					<h3 class="title-d" id="titulo_modulo">Editar asesor</h3>
                    <?php }else{?>
					<h3 class="title-d" id="titulo_modulo">Nuevo asesor</h3>
                    <?php }?>
				</div>

				<form class="form-a" method="POST" action="" enctype="multipart/form-data" id="formulario_noticia">
                    <?php if(isset($F)) echo "<input type='hidden' name='idn' value='".$F['id']."'>";?>
					<div class="row">
						<div class="col-md-4 mb-2">
							<div class="form-group">
								<label for="Título">Imagén (Recomendado 200x200)</label>
								<input type="file" class="form-control " name="img">
                                <?php
                                  if(isset($F) && $F!=null) echo "<div class='text-center' id='cont_img'><a href='".$F['img_age']."' target='__blank'>Imagén adjunta del asesor</a></div>";
                                ?>
							</div>
						</div>
						<div class="col-md-4 mb-2">
							<div class="form-group">
								<label for="Título">Estatus del asesor</label>
                                <select name="est" class="form-control">
                                  <option value="">Seleccione</option>
                                  <option value="1">Activo</option>
                                  <option value="0">Inactivo
                                </select>
							</div>
						</div>
						<div class="col-md-4 mb-2">
							<div class="form-group">
								<label for="Título">Codígo del asesor</label>
								<input type="text" class="form-control form-control-lg form-control-a" placeholder="Codígo del asesor" name="cod" value="<?php echo $F['cod_age'];?>">
							</div>
						</div>
						<div class="col-md-4 mb-2">
							<div class="form-group">
								<label for="Título">Nombre</label>
								<input type="text" class="form-control form-control-lg form-control-a" placeholder="Nombre del asesor" name="nom" value="<?php echo $F['nom_age'];?>">
							</div>
						</div>
						<div class="col-md-4 mb-2">
							<div class="form-group">
								<label for="Título">Apellido</label>
								<input type="text" class="form-control form-control-lg form-control-a" placeholder="Apellido del asesor" name="ape" value="<?php echo $F['ape_age'];?>">
							</div>
						</div>
						<div class="col-md-4 mb-2">
							<div class="form-group">
								<label for="Título">Teléfono</label>
								<input type="text" class="form-control form-control-lg form-control-a" placeholder="Teléfono del asesor" name="tlf" value="<?php echo $F['tlf_age'];?>">
							</div>
						</div>
					</div>
                    <div class="modal-footer">
                        <button type="submit" id="bt_modulo" name="<?php echo ((isset($F))?'btc':'btg')?>" class="btn btn-b"><?php echo ((isset($F))?'Guardar Cambios':'Guardar')?></button>
                    </div>
				</form>
			</div>


		</div>
	</div>
</div>

<?php
  if(isset($F)){
?>
  <script>
    $(document).ready(function(){
      $("#bt_nueva_noticia").trigger('click');
      $("select[name='est']").val(<?php echo $F['est_age'];?>);
    });
  </script>

<?php
  } 
?>
<script>
    $(document).ready(function(){
      $("#bt_nueva_noticia").click(function(){
        $("#titulo_modulo").text("Nueva asesor");
        $("#bt_modulo").attr('name', 'btg');
        $("#bt_modulo").text('Guardar');
        $("input[name='tit']").val('');
        $("input").val('');
        $("textarea[name='des']").val('');
        $("select[name='est']").val('');
        $("#cont_img").remove();
      });
    });

</script>

