<?php
  include_once("modelo/Repuesto.php");

  if(isset($_GET['aprobar'])){
    $idc = $_GET['aprobar'];
    $est = 1;
    $cita = new Repuesto();
    if($cita->cambiarEstatus($est, $idc)!=false){
      $err = "¡Solicitud procesado correctamente!";
      echo "<script>window.location ='?op=solicitud_repuestos&info&msj=$err';</script>";
    }else{
      $err = "No se pudo procesar la solicitud";
      echo "<script>window.location ='?op=solicitud_repuestos&err&msj=$err';</script>";
    }
  }else if(isset($_GET['rechazar'])){
    $idc = $_GET['rechazar'];
    $est = -2;
    $cita = new Repuesto();
    if($cita->cambiarEstatus($est, $idc)){
      $err = "¡Repuesto rechazada correctamente!";
      echo "<script>window.location ='?op=solicitudes&info&msj=$err';</script>";
    }else{
      $err = "No se pudo rechazar la cita!";
      echo "<script>window.location ='?op=solicitudes&err&msj=$err';</script>";
    }
  }else if(isset($_GET['finalizar'])){
    $idc = $_GET['finalizar'];
    $est = 2;
    $cita = new Repuesto();
    if($cita->cambiarEstatus($est, $idc)){
      $err = "¡Cita finalizada correctamente!";
      echo "<script>window.location ='?op=solicitudes&info&msj=$err';</script>";
    }else{
      $err = "No se pudo finalizar la cita!";
      echo "<script>window.location ='?op=solicitudes&err&msj=$err';</script>";
    }
  }else if(isset($_GET['cancelar'])){
    $idc = $_GET['cancelar'];
    $est = -1;
    $cita = new Repuesto();
    if($cita->cambiarEstatus($est, $idc)){
      $err = "¡Cita cancelada correctamente!";
      echo "<script>window.location ='?op=solicitudes&info&msj=$err';</script>";
    }else{
      $err = "No se pudo cancelar la cita!";
      echo "<script>window.location ='?op=solicitudes&err&msj=$err';</script>";
    }
  }

?>

<div class="container mt-5">
	<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h4 class="m-0 font-weight-bold color-b">Solicitudes de repuestos</h4>
	</div>

	<div class="card-body">
              <?php include_once("mensajes.php");?>
        <form action="" method="GET">
          <input type="hidden" name="op" value="solicitud_repuestos">
          <div class="row">
            <?php
              //$primero = date('Y-m-')."01";
              if(isset($_GET['fec1']) && isset($_GET['fec2'])){
                $primero = $_GET['fec1'];
                $ultimo = $_GET['fec2'];
              }else{
                $fa = date('Y-m-d');
                $primero = date("Y-m-d", strtotime($fa. "- 4 days"));
                $a_date = date('Y-m-d');
                //$ultimo = date("Y-m-t", strtotime($a_date));
                $ultimo = date('Y-m-d');
              }
            ?>
            <div class="col-md-3">

              <label>Desde: </label>
              <input type="date" name="fec1" class="form-control" value="<?php echo (!isset($_GET['fec1']))?$primero:$_GET['fec1'];?>">
            </div>
            <div class="col-md-3">
              <label>Hasta: </label>
              <input type="date" name="fec2" class="form-control" value="<?php echo (!isset($_GET['fec2']))?$ultimo:$_GET['fec2']?>">
            </div>
            <div class="col-md-3">
              <label>&nbsp;</label>
              <br>
              <input type="submit" name="btf" class="btn btn-danger" value="Filtrar">
            </div>
          </div>
        </form>
        <hr>
		<div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th>#</th>
						<th>Enviada</th>
						<th>Nombre y apellido</th>
						<th>Teléfono</th>
						<th>Estatus</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody>
	              <?php
	                $noti = new Repuesto();
	                $r = $noti->solicitudesGenerales($primero, $ultimo);
	                $i=0;
	                while($ff = $r->fetch_assoc()){
	                  $i++;
                      if($ff['estatus']==1)
                        $st = "Procesada";
                      else
                        $st = "Pendiente";
	                  echo "<tr>";
	                  echo "  <td>" . $i . "</td>";
                      echo "  <td class='momento1'>" . $ff['fec_reg_sol'] . "</td>";
	                  echo "  <td>" . $ff['nom'] . " ".$ff['ape'];
	                  echo " - " . $ff['cor'] . "</td> ";
	                  echo "  <td>" . $ff['tel'] . " </td>";

                      if($ff['estatus'] == 0)
                        echo "  <td style='color:#fff;background:orange;'>" . $st . " </td>";
                      else
                        echo "  <td style='color:#fff;background:green;'>" . $st . "</td> ";
                      echo "<td class='text-center'>";
                      echo "<a href='#' title='Ver detalles' class='bt_detalle' id='".$ff['id']."'><i class='mr-2 fa fa-eye'></i></a>";
                      if($ff['estatus'] == 0){
                        echo "<a href='?op=solicitud_repuestos&aprobar=".$ff['id']."' title='Procesar solicitud' class='bt_detalle' id='".$ff['id']."'> <i class='mr-2 fa fa-check'></i></a>";
                      }
                      echo "</td>";
	                  echo "</tr>";
	                }
	              ?>
				</tbody>
			</table>
		</div>
	</div>
	</div>
</div>

<div id="md-detalle" class="modal modalmedium fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
                <h5>Solicitud de repuesto</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="font-size:12px;">
				<div class="title-box-d">
                  <form class="form-a" method="POST" action="" enctype="multipart/form-data" id="formulario_modelo">
                      <div class="row">
                          <div class="col-md-2 mb-2">
                              <div class="form-group">
                                  <label for="Modelo"><b>Estatus </b></label>
                              </div>
                          </div>
                          <div class="col-md-4 mb-4">
                              <div class="form-group text-left" id="estatus" style="color:#000;">
                              </div>
                          </div>
                          <div class="col-md-2 mb-2">
                              <div class="form-group">
                                  <label for="Modelo"><b>Enviada</b></label>
                              </div>
                          </div>
                          <div class="col-md-4 mb-4">
                              <div class="form-group momento" id="enviado">
                              </div>
                          </div>
                          <div class="col-md-2 mb-2">
                              <div class="form-group">
                                  <label for="Modelo"><b>Cliente</b></label>
                              </div>
                          </div>
                          <div class="col-md-4 mb-4">
                              <div class="form-group" id="cliente">
                              </div>
                          </div>
                          <div class="col-md-2 mb-2">
                              <div class="form-group">
                                  <label for="Modelo"><b>Teléfono</b></label>
                              </div>
                          </div>
                          <div class="col-md-4 mb-4">
                              <div class="form-group" id="telefono">
                              </div>
                          </div>
                          <div class="col-md-2 mb-2">
                              <div class="form-group">
                                  <label for="Modelo"><b>Correo </b></label>
                              </div>
                          </div>
                          <div class="col-md-4 mb-4">
                              <div class="form-group" id="correo">
                              </div>
                          </div>
                          <div class="col-md-12 mb-2" style="" id="conte_observacion">
                              <label for="Modelo"><b>Descripción del repuesto que necesita el cliente.</b></label>
                              <div class="form-group">
                                <textarea class="form-control" id="observacion"></textarea>
                              </div>
                          </div>
                        
                          <div class="col-md-12 mb-2">
                              <div style="display:none;" class="text-center alert alert-info" id="mensajes"></div>
                              <div style="display:none;" class="img_cargando" id="img_cargando"><img src="../static/img/cargando.gif" style="width:50px;"></div>
                          </div>

                            </div>
                          </div>
                      </div>
                  </form>
				</div>
			</div>
		</div>
	</div>
</div>


<script>
    $(document).ready(function(){
      moment.locale('es');         // en
      $(".momento1").each(function(){
        $(this).text(moment($(this).text()).format('lll'));
      });
      $("#bt_nueva_noticia").click(function(){
        $("#titulo_modulo").text("Nueva cita");
        $("#bt_modulo").attr('name', 'btg');
        $("#bt_modulo").text('Guardar');
        $("input[name='pla']").val('');
        $("input[name='ser1']").val('');
        $("input[name='ser2']").val('');
        $("input[name='ser3']").val('');
        $("input[name='ser4']").val('');
        $("input[name='ano']").val('');
        $("select[name='mod']").val('');
      });

      $(".btop").on('click', function(){
        var idc = $(this).attr('id_cita');
        var idt = $(this).attr('id');
        var obs = $("#observacion").val().trim();
        
        if(confirm('Esta seguro?')){

          var obj = { 
            modulo: 'citas',
            tipo: 'cambiarEstatus',
            idc: idc,
            valor: null,
            ida: '<?php echo $ida;?>',
            obs: obs
          }
          obj.reprogramado=0;
          obj.fecha_nueva = null;
          if(idt == 'bt_aprobar'){
            obj.valor = 1;
            var bt = $("#bt_aprobar");
          }else if(idt == 'bt_reprogramar'){
            obj.valor = 1;
            obj.reprogramado=1;
            obj.fecha_nueva = $("#fecha_nueva").val();
            var bt = $("#bt_aprobar");
            var fcc = $("#fec_cita").attr('fec_cita');
            var fnv = $("#fecha_nueva").val();
            if(fnv <= fcc){
              alert("La nueva fecha debe ser mayor a la actual de la cita.");
              return;
            }
          }else if(idt == 'bt_finalizar'){
            obj.valor = 2;
            var bt = $("#bt_finalizar");
          }else if(idt == 'bt_rechazar'){
            obj.valor = -2;
            var bt = $("#bt_rechazar");
          }else if(idt == 'bt_cancelar'){
            obj.valor = -1;
            var bt = $("#bt_cancelar");
          }

          $(".img_cargando").show();
          $("#mensajes").removeClass("alert-success");
          $("#mensajes").removeClass("alert-danger");
          $("#botones").hide();
          $.post('ajax_php.php', obj, function(data){
            console.log(data);
            $(".img_cargando").hide();
            $("#mensajes").show();
            $("#botones").show();
            if(data.r == true){
              $("#mensajes").addClass("alert-success").text(data.msj);
              actualizarEvento(data);
              bt.hide();
              if(obj.reprogramado == 1){
                $("#contenedor_reprogramar").hide();
              }
            }else
              $("#mensajes").addClass("alert-danger").text(data.msj);
          });
        }
      });

      function actualizarEvento(obj){

        $("#estado_" + obj.cita.id).css('background', obj.cita.color);
        $("#estado_" + obj.cita.id).text(obj.cita.txt);

      }

      $(document).on('click', '.bt_detalle', function(){
        var idd = this.id;
        $("#md-detalle").modal("show");
        $(".img_cargando").show();
        $.post('ajax_php.php', {modulo: 'repuestos', tipo: 'obtenerSolicitud', ids: idd}, function(data){
          console.log(data);
          $(".img_cargando").hide();
          if(data.r == false) {
            alert(data.msj);
            return;
          }
          var dataCita = data.sol;
            if(dataCita.estatus==1)
              $("#estatus").text("Procesada");
            else
              $("#estatus").text("Pendiente");
              $("#cliente").html(dataCita.nom+" "+dataCita.ape);
              $("#enviado").text(dataCita.fec_reg_sol);
              $("#telefono").text(dataCita.tel);
              $("#correo").text(dataCita.cor);
              $("#observacion").text(dataCita.repuesto);
              $("#mensajes").hide();
              $(".btop").each(function(){
                $(this).attr('id_cita', dataCita.idc);
              });
              $(".momento").each(function(){
                $(this).text(moment($(this).text()).format('llll'));
              });
              $(".btop").show();

              if(dataCita.estatus == 0){
              }else if(dataCita.estatus == 1)
                $("#bt_aprobar").hide();
              else if(dataCita.estatus == -1)
                $("#bt_cancelar").hide();
              else if(dataCita.estatus == -2)
                $("#bt_rechazar").hide();
              else if(dataCita.estatus == 2){
                $("#bt_finalizar").hide();
                $("#bt_aprobar").hide();
              }
        });
      });
    });

</script>

